'use strict';

const Sequelize = require('sequelize');
let sequelize = null;
const fs = require('fs');
const path = require('path');

const Camera    = require('./lib/Camera').Camera;
const MPL3115A2 = require('./lib/MPL3115A2').MPL3115A2;
const LIS3DH    = require('./lib/LIS3DH').LIS3DH;
const BMP180    = require('./lib/BMP180').BMP180;
const BMP280    = require('./lib/BMP280').BMP280;
const GPS       = require('./lib/GPS').GPS;
const RTC       = require('./lib/RTC').RTC;

/**
 * The NeoPi class.
 */
class NeoPi {

  /**
   * ctor()
   * @param options
   */
  constructor(options) {
    this._sequelize = new Sequelize('neopi', 'ashlyn', 'I<3science!', {
      host:        'localhost',
      dialect:     'sqlite',
      pool:        {
        max:  5,
        min:  0,
        idle: 10000
      },
      logging:     console.log,
      storage:     './data/neopi.sqlite', // ':memory:',
      transaction: Sequelize.Transaction.EXCLUSIVE, // 'DEFERRED', 'IMMEDIATE', 'EXCLUSIVE'
      benchmark:   true
    });

    const sensors = [
      require('./lib/Camera').Camera,
    ];

    this.camera    = new Camera   ({ db: this._sequelize });
    this.mpl3115a2 = new MPL3115A2({ db: this._sequelize });
    this.lis3dh    = new LIS3DH   ({ db: this._sequelize });
    this.bmp180    = new BMP180   ({ db: this._sequelize });
    this.bmp280    = new BMP280   ({ db: this._sequelize });
    this.gps       = new GPS      ({ db: this._sequelize });
    this.clock     = new RTC      ({ db: this._sequelize });
  }

  /**
   *
   * @param dir
   * @private
   */
  _loadModels(dir) {
    let baseDir = path.join(__dirname, '/lib/model/');
    let files = fs.readdirSync(baseDir);
    for (const file of files) {
      if (fs.lstatSync(path.join(baseDir, file)).isFile()) {
        this._sequelize.import(__dirname + '/lib/model/' + file);
      }
    }

    let models = Object.keys(this._sequelize.models);
    let ops = [];
    for (const model of models) {
      ops.push(this._sequelize.models[model].sync());
    }
    return Promise.all(ops);
  }

}

module.exports.NeoPi = NeoPi;
