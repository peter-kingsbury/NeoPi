'use strict';

const Promise = require('bluebird');
const Armature = require('../lib/armature/Armature').Armature;
const Program = require('../lib/armature/Program').Program;

module.exports = {
  loadPriority:  1300,
  startPriority: 1300,
  stopPriority:  1300,
  initialize: function(api, next) {
    switch (api.mode) {
      case 'PAYLOAD': {
        api.log(`Armature startup`);
        api.armature = new Armature({
          panPin: 12,
          tiltPin: 13,
          photoOutputDir: './data/images',
          cameraRotation: 90
        });

        api.armature._init().then(() => {
          next();
        }).catch(err => {
          next(err);
        });
      } break;

      default: {
        next();
      } break;
    }
  },
  start: function(api, next) {
    switch (api.mode) {
      case 'PAYLOAD': {
        api.armature.runProgram(new Program('zero', 0.5, 0.5), false).then(() => {
          next();
        }).catch(err => {
          next(err);
        });
      } break;

      default: {
        next();
      } break;
    }
  },
  stop: function(api, next) {
    switch (api.mode) {
      case 'PAYLOAD': {
        api.armature.runProgram(new Program('zero', 0.5, 0.5), false).then(() => {
          api.armature._shutdown();
          next();
        }).catch(err => {
          next(err);
        });
      } break;

      default: {
        next();
      } break;
    }
  }
};
