'use strict';

module.exports = {
  loadPriority:  101,
  startPriority: 101,
  stopPriority:  101,

  initialize: function(api, next) {
    api.mode = process.env.MODE;
    switch (api.mode) {
      case 'MISSIONCONTROL': {} break;
      case 'PAYLOAD': {} break;
      default: {
        api.log(`Environment MODE must be one of MISSIONCONTROL or PAYLOAD. Exiting...`, 'notice');
        process.exit(0);
        next();
      } break;
    }

    next();
  },
  start: function(api, next) {
    next();
  },
  stop: function(api, next) {
    next();
  }
};
