'use strict';

module.exports = {
  loadPriority:  999,
  startPriority: 999,
  stopPriority:  999,

  initialize: function(api, next) {
    [
      api.tasks,
      api.cache,
      api.chatRoom
    ].forEach(entry => {
      Promise.promisifyAll(entry);
    });
    next();
  },
  start: function(api, next) {
    next();
  },
  stop: function(api, next) {
    next();
  }
};
