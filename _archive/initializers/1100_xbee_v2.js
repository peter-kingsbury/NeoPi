'use strict';

const XBee = require('../lib/xbee/XBee').XBee;

module.exports = {
  loadPriority:  1100,
  startPriority: 1100,
  stopPriority:  1100,
  initialize: async function(api, next) {
    try {
      api.xbee = new XBee(api.config.xbee.device, api.config.xbee.baudRate);

      api.log(`XBee startup`);

      await api.xbee.initialize();
      await api.xbee.run(api.config.xbee.boot);

      api.xbee.events.on('data', async data => {
        try {
          await api.tasks.enqueueAsync('receiveRadioData', data, 'default');
        } catch (e) {
          api.log(`Error enqueueing task 'receiveRadioData'`, 'error', e);
        }
      });

      next();
    } catch (e) {
      api.log('Error', 'error', e);
      next(e);
    }
  },
  start: function(api, next) {
    next();
  },
  stop: function(api, next) {
    next();
  }
};
