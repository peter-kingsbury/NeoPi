'use strict';

const XBee = require('../lib/xbee/XBee').XBee;

module.exports = {
  loadPriority:  1100,
  startPriority: 1100,
  stopPriority:  1100,
  initialize: function(api, next) {
    return next();

    api.xbee = new XBee(api.config.xbee.device, api.config.xbee.baudRate);

    api.log(`XBee startup`);

    api.xbee.initialize().then(() => {
      return api.xbee.run(api.config.xbee.boot).then(() => {
        api.xbee.events.on('data', data => {
          api.tasks.enqueue('receiveRadioData', data, 'default', err => {
            if (err) {
              api.log(`Error enqueueing task 'receiveRadioData'`, 'error', err);
            }
          });
        });
        next();
      });
    }).catch(err => {
      console.trace(err);
      next(err);
    });
  },
  start: function(api, next) {
    next();
  },
  stop: function(api, next) {
    next();
  }
};
