'use strict';

module.exports = {
  loadPriority: 1000,
  startPriority: 1000,
  stopPriority: 1000,
  initialize: function(api, next) {
    api.process = {};

    // process.on('unhandledRejection', (reason, p) => {
    //   console.trace(p);
    //   console.trace(reason);
    // });
    //
    // process.on('uncaughtException', err => {
    //   console.trace(err);
    // });

    next();
  },
  start: function(api, next) {
    next();
  },
  stop: function(api, next) {
    next();
  }
};
