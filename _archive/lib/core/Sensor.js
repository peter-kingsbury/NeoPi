'use strict';

const EventEmitter = require('events').EventEmitter;
const retry = require('retry-as-promised');

class Sensor {
  constructor(options) {
    if (!options) {
      throw new Error('invalid options');
    }

    if (!options.db) {
      throw new Error('invalid db');
    }

    if (!options.cache) {
      throw new Error('invalid cache');
    }

    this._db = options.db;
    this._cache = options.cache;
    this._id = {
      id:       null,
      property: {}
    };
    this.dbLog = null;
    this.events = new EventEmitter();
  }

  /**
   * Rudimentary cache for Sensor ID.
   * @param {Object} t
   * @return {Promise.<T>}
   * @private
   */
  _getSensorId(t = null) {
    if (this._id.sensor) {
      return Promise.resolve(this._id.sensor);
    }

    return this._db.models.sensor.findOrCreate({
      where:       {
        name: this.constructor.name
      },
      defaults:    {
        name: this.constructor.name
      },
      transaction: t
    }).spread((result, created) => {
      let sensorType = result.get({plain: true});
      this._id.sensor = sensorType.id;
      return this._id.sensor;
    });
  }

  /**
   * Wrapper for creating a new Reading record.
   * @param {Object} t
   * @param {Number} sensorId
   * @return {Promise.<T>}
   * @private
   */
  _createReading_v1(t, sensorId) {
    return this._db.models.reading.create({
      timestamp: new Date(),
      sensor_id: sensorId
    }, { transaction: t }).then(result => {
      let reading = result.get({plain: true});
      return reading.id;
    });
  }

  async _createReading(t, sensorId) {
    let result = await this._db.models.reading.create({
      timestamp: new Date(),
      sensor_id: sensorId
    }, { transaction: t });

    let reading = result.get({plain: true});
    return reading.id;
  }

  /**
   * Rudimentary cache for Property ID.
   * @param {Object} t
   * @param {String} key
   * @return {*}
   * @private
   */
  _getPropertyId(t, key) {
    if (this._id.property[key]) {
      return Promise.resolve(this._id.property[key]);
    } else {
      return this._db.models.property.findOrCreate({
        where: {
          name: key
        },
        defaults: {
          name: key
        },
        transaction: t
      }).spread((result, created) => {
        let property = result.get({plain: true});
        this._id.property[key] = property.id;
        return this._id.property[key];
      });
    }
  }

  /**
   * Store a set of readings for this sensor, given the timestamp. Wraps other implementations.
   * @param {Object} properties The properties to be stored.
   * @return {*}
   */
  store(properties) {
    return retry(() => {
      return this.cacheReading(properties);
    }, {
      max: 3,
      timeout: 10000,
      match: [
        this._db.Sequelize.TimeoutError,
        'SQLITE_BUSY'
      ],
      backoffBase: 125,
      backoffExponent: 1.1
    }).then(reading => {
      this.events.emit('reading', reading);
    });
  }

  /**
   * Optimized version of the store function.
   * @param {Object} properties The properties to be stored.
   * @return {*}
   */
  optimalStore(properties) {
    return this._db.transaction(t => {
      return this._getSensorId(t).then(sensorId => {
        return this._createReading(t, sensorId).then(readingId => {
          const keys = Object.keys(properties);
          let ops = [];
          for (const key of keys) {
            let value = properties[key];
            ops.push(this._getPropertyId(t, key).then(propertyId => {
              return this._db.models.reading_property.create({
                reading_id: readingId,
                property_id: propertyId,
                value: value
              }, { transaction: t });
            }));
          }
          return Promise.all(ops);
        });
      });
    });
  }

  /**
   * Enqueue a reading into a list in the cache.
   * @param {Object} properties
   * @return {Promise}
   */
  cacheReading(properties) {
    return new Promise((resolve, reject) => {
      return Promise.resolve().then(() => {
        if (this._id.sensor) {
          return this._id.sensor;
        }
        // return this._db.transaction(t => {
        return this._getSensorId(t);
      });
      return this._getSensorId();
    }).then(sensorId => {
      let now = Date.now();
      let reading = {
        name:      this.constructor.name,
        sensorId,
        timestamp: now,
        data:      properties
      };
      this._cache.push(`readings`, reading, err => {
        if (err) {
          return reject(err);
        }
        resolve(reading);
      });
    }).catch(err => {
      reject(err);
    });
  }

  /**
   * Store a single sensor reading, plus all its associated reading properties.
   * @param {Object} t
   * @param {Object} reading
   * @return {Promise}
   */
  storeReading(t, reading) {
    // Create the reading record
    return this._db.models.reading.create({
      timestamp: reading.timestamp,
      sensor_id: reading.sensorId
    }, { transaction: t }).then(result => {
      let reading = result.get({plain: true});
      return reading.id;
    }).then(readingId => {
      let properties = reading.data;
      const keys = Object.keys(properties);
      let ops = [];
      for (const key of keys) {
        let value = properties[key];
        ops.push(this._getPropertyId(t, key).then(propertyId => {
          return this._db.models.reading_property.create({
            reading_id:  readingId,
            property_id: propertyId,
            value:       value
          }, {transaction: t});
        }));
      }
      return Promise.all(ops);
    });
  }
}

module.exports.Sensor = Sensor;
