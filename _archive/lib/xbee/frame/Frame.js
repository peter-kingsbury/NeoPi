'use strict';

class Frame {
  constructor() {
    this._overhead = 18;
    this.buffer = new Buffer(274);
  }
}

module.exports.Frame = Frame;
