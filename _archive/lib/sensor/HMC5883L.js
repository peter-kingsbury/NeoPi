'use strict';

const Sensor = require('../core/Sensor').Sensor;
const Compass = require('compass-hmc5883l');

class HMC5883L extends Sensor {
  constructor(options) {
    super(options);
    this.address = 0x1E;
    this.name = 'HMC5883L';
    this._sensor = new Compass(1, {
      sampleRate: 75
    });
  }

  _init() {
    return Promise.resolve();
  }

  readHeading() {
    return new Promise((resolve, reject) => {
      this._sensor.getHeadingDegrees('x', 'y', (err, heading) => {
        if (err) {
          reject(err);
        } else {
          resolve(heading);
        }
      });
    });
  }

  readRawValues() {
    return new Promise((resolve, reject) => {
      this._sensor.getRawValues((err, values) => {
        if (err) {
          reject(err);
        } else {
          resolve(values);
        }
      });
    });
  }
}

module.exports.HMC5883L = HMC5883L;
