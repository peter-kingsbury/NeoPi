'use strict';

const Bluebird = require('bluebird');
const Lcd = require('lcd');

class LCD {
  constructor(rs = 27, e = 22, data = [25, 24, 23, 18], cols = 16, rows = 2) {
    this._lcd = Bluebird.promisifyAll(new Lcd({ rs, e, data, cols, rows }));
  }

  init() {
    return new Promise((resolve, reject) => {
      resolve();
    });
  }

  print(text) {
    return this._lcd.printAsync(text);
/*

    return new Promise((resolve, reject) => {
      this._lcd.print(text, (err, str) => {
        if (err) {
          reject(err);
        } else {
          resolve(str);
        }
      });
    });
*/
  }

  async clear2() {
    await this._lcd.clearAsync();
  }

  clear() {
    return new Promise((resolve, reject) => {
      this._lcd.clear(err => {
        if (err) {
          reject(err);
        } else {
          resolve(true);
        }
      });
    });
  }

}

module.exports.LCD = LCD;

let lcd = new LCD();
lcd._lcd.on('ready', () => {
  lcd._lcd.setCursor(0, 0);
  lcd._lcd.noAutoscroll();
  lcd._lcd.clearAsync().then(() => {
    return lcd._lcd.printAsync("NeoPi - An").then(() => {
      lcd._lcd.setCursor(0, 1);
      return lcd._lcd.printAsync("Ashlyn Project");
    });
  });
});

