'use strict';

class Util {

  /**
   * Calculate the distance (in metres) of a host.
   * @param rssi The receive signal strength indicator of the the receiver
   * @param txPower The power of the transmitter in dBm
   * @returns {number} The distance in metres.
   */
  static calculateDistance(rssi, txPower = 24) {
    if (rssi === 0) {
      return -1.0;
    }

    let ratio = rssi * 1.0 / txPower;
    if (ratio < 1.0) {
      return Math.pow(ratio, 10);
    }
    else {
      return (0.89976)*Math.pow(ratio, 7.7095) + 0.111;
    }
  }

  static geoDistance(lat1, lon1, lat2, lon2, unit = 'K') {
    let radlat1 = Math.PI * lat1 / 180;
    let radlat2 = Math.PI * lat2 / 180;
    let theta = lon1 - lon2;
    let radtheta = Math.PI * theta / 180;
    let dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
    dist = Math.acos(dist);
    dist = dist * 180 / Math.PI;
    dist = dist * 60 * 1.1515;
    if (unit.toUpperCase() === 'K') {
      dist = dist * 1.609344;
    }
    if (unit.toUpperCase() === 'N') {
      dist = dist * 0.8684;
    }
    return dist;
  };
}

module.exports.Util = Util;
