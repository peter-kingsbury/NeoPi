'use strict';

const OS = require('os');
const Bluebird = require('bluebird');
const diskspace = Bluebird.promisifyAll(require('diskspace'));

let oldCPUTime = 0;
let oldCPUIdle = 0;

function getLoad() {
  let cpus = OS.cpus();
  let totalTime = -oldCPUTime;
  let totalIdle = -oldCPUIdle;
  for (let i = 0; i < cpus.length; i++) {
    let cpu = cpus[i];
    for (const type of Object.keys(cpu.times)) {
      totalTime += cpu.times[type];
      if (type === 'idle') {
        totalIdle += cpu.times[type];
      }
    }
  }

  oldCPUTime = totalTime;
  oldCPUIdle = totalIdle;

  return {
    CPU: 1 - (totalIdle / totalTime),
    mem: 1 - (OS.freemem() / OS.totalmem())
  };
}

function readSystemInfo() {
  return diskspace.checkAsync('/').then(disk => {
    let load = getLoad();
    return {
      name:      'System',
      sensorId:  0,
      timestamp: Date.now(),
      data: {
        cpu:    load.CPU,
        memory: load.mem,
        disk:   disk.free / disk.total
      }
    };
  });
}

exports.task = {
  name: 'readSystemInfo',
  description: 'Read system information',
  frequency: 2500,
  queue: 'payload',
  middleware: [],

  run: function(api, params, next) {
    let start = Date.now();
    readSystemInfo().then(reading => {
      api.log(`Read System Info (${Date.now() - start} ms)`);
      return api.tasks.enqueueAsync('transmitRadioData', reading, 'default');
    }).then(() => {
      next();
    }).catch(err => {
      api.log(err.message, 'error');
      next();
    });
  }
};
