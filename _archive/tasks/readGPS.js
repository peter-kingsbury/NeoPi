'use strict';

const Util = require('../lib/util/Util').Util;
const ACCEPTABLE_DISTANCE_THRESHOLD = 1000;
const Home = {
  latitude: 43.383244,
  longitude: -80.310722
};
let lastGps = Home;

exports.task = {
  name: 'readGPS',
  description: 'My Task',
  frequency: 1000,
  queue: 'payload',
  middleware: [],

  run: function(api, params, next) {
    if (!api.sensors.GPS) return next();

    let start = Date.now();
    Promise.all([
      api.sensors.GPS.read()
    ]).then(result => {
      let gps = result[0];

      if (lastGps === null) {
        lastGps = gps;
      } else {
        let distanceFromHome = Util.geoDistance(Home.latitude, Home.longitude, gps.latitude, gps.longitude);
        distanceFromHome = Math.round(distanceFromHome * 100) / 100;
        if (distanceFromHome > ACCEPTABLE_DISTANCE_THRESHOLD) {
          // api.log(`Distance from home: ${distanceFromHome} km -  WE'RE NOT IN KANSAS ANYMORE!`, 'error');
          return next();
        } else {
//          api.log(`Distance from home: ${distanceFromHome} km`, 'notice');
          let distanceDelta = Util.geoDistance(lastGps.latitude, lastGps.longitude, gps.latitude, gps.longitude);
          distanceDelta = Math.round(distanceDelta * 100) / 100;
//          api.log(`Distance since last reading: ${distanceDelta} km`, 'notice');

          api.log(`Distance: ${distanceDelta} km (from home) ${distanceFromHome} km (delta)`);

          lastGps = gps;
        }
      }

      return api.sensors.GPS.store(gps).then(() => {

        api.readings = Object.assign(api.readings, gps);

        // let keys = Object.keys(gps);
        // for (const key of keys) {
        //   if (gps[key]) {
        //     api.readings[key] = gps[key];
        //   }
        // }

        api.log(`Read GPS (${Date.now() - start} ms)`);
        api.log(`Latitude: ${api.readings.latitude} Longitude: ${api.readings.longitude}`);
        next();
      });
    }).catch(err => {
      api.log(`Failed GPS read (${err.message}) (${Date.now() - start} ms)`, 'error');
      next();
    });
  }
};
