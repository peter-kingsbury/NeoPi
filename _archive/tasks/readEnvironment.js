'use strict';

exports.task = {
  name: 'readEnvironment',
  description: 'My Task',
  frequency: 2500,
  queue: 'payload',
  middleware: [],

  run: function(api, params, next) {
    if (!api.sensors.Environment) return next();

    let start = Date.now();
    Promise.all([
      api.sensors.Environment.read()
    ]).then(result => {
      let env = result[0];
      let data = {
        temperature: env.temperature_C,
        humidity: env.humidity,
        pressure: env.pressure_hPa,
        altitude: env.altitude
      };
      return api.sensors.Environment.store(data).then(() => {
        Object.assign(api.readings, data);
        api.log(`Read Environment (${Date.now() - start} ms)`);
        next();
      });
    }).catch(err => {
      api.log(`Failed Environment read (${err.message}) (${Date.now() - start} ms)`, 'error');
      next();
    });
  }
};
