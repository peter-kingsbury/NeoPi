'use strict';

exports.task = {
  name:        'heartbeat',
  description: 'My Task',
  frequency:   5000,
  queue:       'heartbeat',
  middleware:  [],

  run: function (api, params, next) {
    api.log(`♥`, 'info');

    if (process.env.MODE === 'PAYLOAD') {
      let heartbeat = {
        name: 'heartbeat',
        timestamp: Date.now(),
        data: {}
      };

      // api.tasks.enqueue
      api.tasks.enqueueAsync('transmitRadioData', heartbeat, 'default').then(() => {
        next();
      }).catch(err => {
        next(err);
      });
    } else {
      next();
    }
  }
};
