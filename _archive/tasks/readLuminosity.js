'use strict';

exports.task = {
  name: 'readLuminosity',
  description: 'My Task',
  frequency: 2500,
  queue: 'payload',
  middleware: [],

  run: function(api, params, next) {
    if (!api.sensors.Luminosity) return next();

    let start = Date.now();
    Promise.all([
      api.sensors.Luminosity.readLuminosity()
    ]).then(result => {
      let visibility = result[0].vis_ir;
      let ir = result[0].ir;

      return api.sensors.Luminosity.store({
        visibility,
        ir
      }).then(() => {
        Object.assign(api.readings, {
          visibility,
          ir
        });

        api.log(`Read Luminosity (${Date.now() - start} ms)`);
        next();
      });
    }).catch(err => {
      api.log(`Failed Luminosity read (${Date.now() - start} ms)`, 'error');
      next();
    });
  }
};
