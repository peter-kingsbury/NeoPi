'use strict';

const FrameBuilder = require('../lib/xbee/frame/FrameBuilder').FrameBuilder;

exports.task = {
  name: 'transmitRadioData',
  description: 'My Task',
  frequency: 0,
  queue: 'default',
  middleware: [],

  run: function(api, params, next) {
    let fb = new FrameBuilder();
    let data = params.data;

    let buffer = fb.build(0x10, {
      destination64: api.config.xbee.destination,
      destination16: 'fffe',
      data: JSON.stringify(Object.assign(data, {
        name: params.name,
        time: params.timestamp
      }))
    });

    api.xbee.sendBuffer(buffer).then(() => {
      next();
    }).catch(err => {
      next(err);
    });
  }
};
