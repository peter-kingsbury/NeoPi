'use strict';

exports.task = {
  name: 'readCompass',
  description: 'My Task',
  frequency: 1000,
  queue: 'payload',
  middleware: [],

  run: function(api, params, next) {
    if (!api.sensors.Compass) return next();
    let start = Date.now();

    Promise.all([
      api.sensors.Compass.readHeading(),
      api.sensors.Compass.readRawValues()
    ]).then(result => {
      let heading = result[0];
      let raw = result[1];
      // api.log(`Heading: ${heading}`);
      return api.sensors.Compass.store({
        heading,
        x: raw.x,
        y: raw.y,
        z: raw.z
      }).then(() => {
        Object.assign(api.readings, {
          heading,
          compass_x: raw.x,
          compass_y: raw.y,
          compass_z: raw.z
        });

        // api.log(`Done reading Compass`);
        api.log(`Read Compass (${Date.now() - start} ms)`);
        next();
      });
    }).catch(err => {
      api.log(`Failed Compass read (${err.message}) (${Date.now() - start} ms)`, 'error');
      next();
    });
  }
};
