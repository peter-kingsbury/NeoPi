'use strict';

const i2c = require('bluebird').promisifyAll(require('i2c'));
const Sensor = require('./Sensor').Sensor;

const COMMAND = {

};

class I2CSensor extends Sensor {
  constructor(options) {
    super(options);

    if (!options.hasOwnProperty('address')) {
      throw new Error(`invalid address ${options.address}`)
    }

    if (!options.hasOwnProperty('device')) {
      throw new Error(`invalid device ${options.device}`)
    }

    this._address = options.address;
    this._device  = options.device;

    this._i2c = new i2c(this._address, {
      device: this._device
    });

    //this._i2c.on('data', data => {
    //  console.log(data);
    //});

    //this._i2c.stream()
  }

  read() {
    return new Promise((resolve, reject) => {

    });
  }

}

module.exports.I2CSensor = I2CSensor;

