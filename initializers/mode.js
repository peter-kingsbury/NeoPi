'use strict';
const {Initializer, api} = require('actionhero');

module.exports = class ModeInitializer extends Initializer {
  constructor() {
    super();
    this.name = 'mode';
    this.loadPriority = 100;
    this.startPriority = 100;
    this.stopPriority = 100;
  }

  async initialize() {
    api.log(`${this.loadPriority} ${this.name} initializing`);
    api['mode'] = process.env.MODE;
  }

  async start() {
    switch (api['mode']) {
      case 'MISSIONCONTROL': {
        api.log(`Starting NeoPi in MISSIONCONTROL mode.`, 'notice');
      } break;
      case 'PAYLOAD': {
        api.log(`Starting NeoPi in PAYLOAD mode.`, 'notice');
      } break;
      default: {
        api.log(`Environment MODE must be one of MISSIONCONTROL or PAYLOAD. Exiting...`, 'notice');
        process.exit(0);
      } break;
    }
  }
  async stop() {}
};
