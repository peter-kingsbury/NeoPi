'use strict';

const EventEmitter = require('events').EventEmitter;
const retry = require('retry-as-promised');

class Sensor {
  constructor(options) {
    if (!options) {
      throw new Error('invalid options');
    }

    if (!options.db) {
      throw new Error('invalid db');
    }

    if (!options.cache) {
      throw new Error('invalid cache');
    }

    if (!options.packetId) {
      throw new Error('invalid packetId');
    }

    this._db = options.db;
    this._cache = options.cache;
    this._id = {
      id:       null,
      property: {}
    };
    this.packetId = options.packetId;
    this.dbLog = null;
    this.events = new EventEmitter();
  }

  /**
   * Rudimentary cache for Sensor ID.
   * @param {Object} t
   * @return {Promise.<T>}
   * @private
   */
  _getSensorId(t) {
    if (this._id.sensor) {
      return Promise.resolve(this._id.sensor);
    }

    return this._db.models.sensor.findOrCreate({
      where:       {
        name: this.constructor.name
      },
      defaults:    {
        name: this.constructor.name
      },
      transaction: t
    }).spread((result, created) => {
      let sensorType = result.get({plain: true});
      this._id.sensor = sensorType.id;
      return this._id.sensor;
    });
  }

  /**
   * Wrapper for creating a new Reading record.
   * @param {Object} t
   * @param {Number} sensorId
   * @return {Promise.<T>}
   * @private
   */
  _createReading(t, sensorId) {
    return this._db.models.reading.create({
      timestamp: new Date(),
      sensor_id: sensorId
    }, { transaction: t }).then(result => {
      let reading = result.get({plain: true});
      return reading.id;
    })
  }

  /**
   * Rudimentary cache for Property ID.
   * @param {Object} t
   * @param {String} key
   * @return {*}
   * @private
   */
  _getPropertyId(t, key) {
    if (this._id.property[key]) {
      return Promise.resolve(this._id.property[key]);
    } else {
      return this._db.models.property.findOrCreate({
        where: {
          name: key
        },
        defaults: {
          name: key
        },
        transaction: t
      }).spread((result, created) => {
        let property = result.get({plain: true});
        this._id.property[key] = property.id;
        return this._id.property[key];
      });
    }
  }

  /**
   * Store a set of readings for this sensor, given the timestamp. Wraps other implementations.
   * @param {Object} properties The properties to be stored.
   */
  async store(properties) {
    return this.store_v3(properties);
  }

  async store_v1(properties) {
    await this.cacheReading(properties);
    // this.events.emit('reading', {
    //   packetId: this.packetId,
    //   data: properties
    // });
  }

  async store_v2(properties) {
    let t;
    let start = Date.now();
    try {
      t = await this._db.transaction();

      let sensorId = await this._getSensorId(t);

      console.log(`Create reading sensorId=${sensorId}`);
      let readingId = await this._createReading(t, sensorId);

      let keys = Object.keys(properties);

      for (const key of keys) {
        let value = properties[key];

        let propertyId = await this._getPropertyId(t, key);

       /* return retry(() => {
          return this.cacheReading(properties);
        }, {
          max: 3,
          timeout: 10000,
          match: [
            this._db.Sequelize.TimeoutError,
            'SQLITE_BUSY'
          ],
          backoffBase: 125,
          backoffExponent: 1.1
        }).then(reading => {
          this.events.emit('reading', reading);
        });*/

        console.log(`Create reading_property readingId=${readingId} propertyId=${propertyId}`);
        await retry(() => {
          return this._db.models.reading_property.create({
            reading_id: readingId,
            property_id: propertyId,
            value: value
          }, { transaction: t }).catch(err => {
            console.trace(err);
            console.log(err.message);
            throw err;
          });
        }, {
          max: 15,
          timeout: 20000,
          match: [
            this._db.Sequelize.TimeoutError,
            'SQLITE_BUSY',
            'SQLITE_BUSY: database is locked'
          ],
          backoffBase: 125,
          backoffExponent: 1.1
        });

       /* await this._db.models.reading_property.create({
          reading_id: readingId,
          property_id: propertyId,
          value: value
        }, { transaction: t });*/
      }

      await t.commit();

      let end = Date.now() - start;

      this.events.emit('reading', {
        packetId: this.packetId,
        data: properties
      });

      // console.log(`Wrote ${keys.length} readings for ${this.name} in ${end} ms`);
    } catch (e) {
      if (t) {
        t.rollback();
      }
      console.trace(e);
      throw e;
    }
  }

  async store_v3(properties) {
    let t;
    // let start = Date.now();
    try {
      t = await this._db.transaction();

      let sensorId = await this._getSensorId(t);

      // console.log(`Create reading sensorId=${sensorId}`);
      let readingId = await this._createReading(t, sensorId);

      let keys = Object.keys(properties);

      for (const key of keys) {
        let value = properties[key];

        let propertyId = await this._getPropertyId(t, key);

        // console.log(`Create reading_property readingId=${readingId} propertyId=${propertyId} value=${value}`);
        await this._db.models.reading_property.create({
          reading_id: readingId,
          property_id: propertyId,
          value: value
        }, { transaction: t })
      }

      await t.commit();

      this.events.emit('reading', {
        packetId: this.packetId,
        data: properties
      });

      // console.log(`Wrote ${keys.length} readings for ${this.name} in ${end} ms`);
    } catch (e) {
      if (t) {
        t.rollback();
      }
      console.trace(e);
      throw e;
    }
  }

  /**
   * Optimized version of the store function.
   * @param {Object} properties The properties to be stored.
   * @return {*}
   */
  optimalStore(properties) {
    return this._db.transaction(t => {
      return this._getSensorId(t).then(sensorId => {
        return this._createReading(t, sensorId).then(readingId => {
          const keys = Object.keys(properties);
          let ops = [];
          for (const key of keys) {
            let value = properties[key];
            ops.push(this._getPropertyId(t, key).then(propertyId => {
              return this._db.models.reading_property.create({
                reading_id: readingId,
                property_id: propertyId,
                value: value
              }, { transaction: t });
            }));
          }
          return Promise.all(ops);
        });
      });
    });
  }

  /**
   * Enqueue a reading into a list in the cache.
   * @param {Object} properties
   * @return {Promise}
   */
  async cacheReading(properties) {
    let sensorId;

    if (this._id.sensor) {
      sensorId = this._id.sensor;
    } else {
      sensorId = await this._getSensorId(null);
    }

    let now = Date.now();
    let reading = {
      name:      this.constructor.name,
      sensorId,
      timestamp: now,
      data:      properties
    };

    await this._cache.push(`readings`, reading);
  }

  /**
   * Store a single sensor reading, plus all its associated reading properties.
   * @param {Object} reading
   */
  async storeReading(reading) {
    // Create the reading record
    let result = await this._db.models.reading.create({
      timestamp: reading.timestamp,
      sensor_id: reading.sensorId
    });

    let readingId = result.get({plain: true}).id;
    let properties = reading.data;

    const keys = Object.keys(properties);
    for (const key of keys) {
      let value = properties[key];
      let propertyId = await this._getPropertyId(null, key);
      await this._db.models.reading_property.create({
        reading_id:  readingId,
        property_id: propertyId,
        value:       value
      });
    }
  }
  async storeReading_v2(reading) {
    let t;
    try {
      t = await this._db.transaction();
      let sensorId = await this._getSensorId(t);
      let readingId = await this._createReading(t, sensorId);
      let properties = reading.data;
      let keys = Object.keys(properties);
      for (const key of keys) {
        let value = properties[key];
        let propertyId = await this._getPropertyId(t, key);
        this._db.models.reading_property.create({
          reading_id: readingId,
          property_id: propertyId,
          value: value
        }, { transaction: t })
      }
      if (t) {
        await t.commit();
      }
    } catch (e) {
      if (t) {
        await t.rollback();
      }
    }

    // Create the reading record
    let result = await this._db.models.reading.create({
      timestamp: reading.timestamp,
      sensor_id: reading.sensorId
    });

    let readingId = result.get({plain: true}).id;
    let properties = reading.data;

    const keys = Object.keys(properties);
    for (const key of keys) {
      let value = properties[key];
      let propertyId = await this._getPropertyId(null, key);
      await this._db.models.reading_property.create({
        reading_id:  readingId,
        property_id: propertyId,
        value:       value
      });
    }
  }
}

module.exports.Sensor = Sensor;
