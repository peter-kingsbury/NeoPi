'use strict';

const EventEmitter = require('events').EventEmitter;
const SerialPort = require('serialport');
const Validator = require('is-my-json-valid');
const util = require('util');

const XBEE_COMMAND_DELAY = 1200;
const MAXIMUM_RECIEVE_BUFFER_SIZE = 512;

/*
<Buffer 7e 00 1d 90 00 13 a2 00 41 5b 3c a7 ff fe c1 05 43 91 7b f8 43 0c>
<Buffer 28 f6 c3 b6 dd 71 c4 12 ba e1 8c>
*/

function getPacketFromBuffer(buffer) {
  try {
    // Start delimiter (1 byte)

    let start = buffer.indexOf(0x7e, 0, 'binary');
    if (start === -1) {
      return null;
    }
    let offset = start + 1;

    // Length (2 bytes)

    let size = buffer.readInt16BE(offset);
    // console.log(`Packet size=${size} bytes`);
    offset += 2;

    let payloadSize = size - 12;
    // console.log(`payloadSize=${payloadSize} bytes`);

    // Frame type (1 byte) should be 0x90

    let frameType = buffer.readUInt8(offset);
    // console.log(`frameType=${frameType}`);
    offset += 1;

    // 64-bit source address (8 bytes)

    let destination64 = Buffer.alloc(8);
    buffer.copy(destination64, 0, offset);
    // console.log(`destination64=${destination64.toString('hex')}`);
    offset += 8;

    // 16-bit source address (2 bytes)

    let destination16 = Buffer.alloc(2);
    buffer.copy(destination16, 0, offset);
    // console.log(`destination16=${destination16.toString('hex')}`);
    offset += 2;

    // Receive options (1 byte)

    let options = buffer.readUInt8(offset);
    // console.log(`options=${options}`);
    offset += 1;

    // RF data (? bytes)

    let payload = Buffer.alloc(payloadSize);
    // console.log(`payload from ${offset} to ${offset + payloadSize}`);
    buffer.copy(payload, 0, offset, offset + payloadSize);
    offset += payloadSize;
    // console.log(payload);

    // Checksum (1 byte)

    let checksum = buffer.readUInt8(offset);
    // console.log(`checksum=${checksum}`);
    offset++;

    return {
      start: '~',
      size,
      frameType,
      destination64,
      destination16,
      options,
      payload,
      checksum,
      newStart: offset
    };
  } catch (e) {
    // Packet didn't work out
    // Return gracefully
  }
}

let validators = {
  program: Validator({
    required: true,
    type: 'array',
    items: {
      type: 'object',
      additionalProperties: false,
      properties: {
        cmd: {
          type: 'string'
        },
        arg: {
          type: 'string'
        }
      },
      required: ['cmd', 'arg']
    }
  }, {
    greedy: true
  })
};

class XBee {
  constructor(device, baudRate = 9600) {
    this._device = device;
    this._baudRate = baudRate;
    this._port = null;
    this._buffer = Buffer.alloc(0);
    this._log = console.log;
    this.events = new EventEmitter();
    this._commandMode = false;
  }

  initialize() {
    this._port = new SerialPort(this._device, {
      baudRate: this._baudRate,
      autoOpen: false
    });

    for (const f of [
      'open',
      'write',
      'drain',
      'pause',
      'resume',
      'write',
      'read'
    ]) {
      this._port[`${f}Async`] = util.promisify(this._port[f]);
    }

    // this._port = Bluebird.promisifyAll(this._port);

    this._port.on('open',       this.onOpen.bind(this));
    this._port.on('data',       this.onData.bind(this));
    this._port.on('close',      this.onClose.bind(this));
    this._port.on('error',      this.onError.bind(this));
    this._port.on('disconnect', this.onDisconnect.bind(this));

    return this._port.openAsync();
  }

  get commandMode() {
    return this._commandMode;
  }

  set commandMode(value) {
    this._commandMode = value;
  }

  validateProgram(program) {
    validators.program(program);
    if (validators.program.errors) {
      throw new Error(`Program error: ${validators.program.errors.field} ${validators.program.errors.message}`);
    }
  }

  async _serialRunProgramEntries(program) {
    let entry = null;
    while (entry = program.shift()) {
      await this.command(entry.cmd, entry.arg);
    }
  }

  async run(program) {
    this.validateProgram(program);
    await this.enterCommandMode();
    await this._serialRunProgramEntries(program);
    await this.exitCommandMode();
  }

  async close() {
    await this._port.drainAsync();
    await this.pause(100);
    await this._port.closeAsync();
  }

  delay(ms) {
    return new Promise(resolve => {
      setTimeout(resolve, ms);
    });
  }

  async pause() {
    this._port.pause();
  }

  async resume() {
    this._port.resume();
  }

  onOpen() {
    this._log(`${this._device}::open`);
  }

  onData(data = '') {
    // If we're in command mode, ignore data events
    if (this._commandMode === true) {
      return;
    }

    this._buffer = Buffer.concat([this._buffer, data]);

    let packet = getPacketFromBuffer(this._buffer);
    if (packet) {
      // console.log(`we got packet: ${JSON.stringify(packet, null, 2)}`)
      this._buffer = this._buffer.slice(packet.newStart);
      this.events.emit('data', packet.payload);
    }

    return;

    // Find the start of 7e

    let start = this._buffer.indexOf(0x7e, 0, 'binary');
    if (start === -1) {
      return;
    }

    let end = this._buffer.indexOf(0x7e, start + 1, 'binary');
    if (end === -1) {
      return;
    }
/*

<Buffer 7e 00 19 90 00 13 a2 00>
<Buffer 41 5b 3c a7 ff fe c1 06 3d 00 00 00 bd 40 00 00 3f 60 00 00 9e>

*/
    console.log(data);

    let complete = this._buffer.slice(start, end);

    this.events.emit('data', this._buffer.slice(start, end));

    this._buffer = this._buffer.slice(end);

    this.events.emit('data', complete);
    return;

    console.log(data.toString('binary'));

    // Sometimes the data contains 'OK'
    if (data[0] === 'O' && data[1] === 'K') {
      data = data.substring(2);
    }

    // Sometimes the buffer contains 'OK'
    if (this._buffer[0] === 'O' && this._buffer[1] === 'K') {
      this._buffer = this._buffer.substring(2);
    }

    // Clean up the incoming string
    data = data.toString().trim().replace('\n', '');

    // Append the data to our buffer
    this._buffer += data;

    if (this._buffer.length > MAXIMUM_RECIEVE_BUFFER_SIZE) {
      let packetCount = this._buffer.split('~').length;
      if (packetCount > 1) {
        console.log(`Found ${packetCount} packets!`);
      }

      console.log(`ALERT: Receive buffer is ${this._buffer.length} bytes long; resetting...`);
      // console.log(this._buffer);
      this._buffer = '';
    }

    if (this._buffer[this._buffer.length - 1] === '}') {
      let messages = this._buffer.replace(/}{/g, '}\n{').split('\n');
      for (const message of messages) {
        try {
          this.events.emit('data', JSON.parse(message));
        } catch (e) {
          console.log(e.message);
          console.log(this._buffer);
        }
      }
      this._buffer = '';
    }
  }

  onClose() {
    this._log(`${this._device}::close`);
  }

  onError(err) {
    this._log(`${this._device}::error ${err.message}`);
  }

  onDisconnect(err) {
    this._log(`${this._device}::disconnect ${err.message}`);
  }

  async writeRaw(data = '', crlf = true) {
    await this._port.writeAsync(crlf ? data + '\r' : data);
  }

  async readRaw(size = null) {
    return await this._port.read(size);
  }

  async sendBuffer(buffer) {
    await this._port.writeAsync(buffer, 'binary');
  }

  async flush() {
    await this._port.flushAsync();
  }

  async enterCommandMode() {
    this._commandMode = true;
    await this.command('+++');
  }

  async exitCommandMode() {
    this._commandMode = false;
    await this.command('ATCN', '');
    await this.resume();
  }

  async command(cmd, arg) {
    let verifyResponse = arg;
    let cmdMode = false;
    if (cmd === '+++') {
      cmdMode = true;
      verifyResponse = 'OK';
    }

    if (cmdMode) {
      await this.delay(1000);
    }

    await this.pause();

    console.log(`${cmd} ${arg ? arg : ''}`);
    if (cmdMode) {
      await this.writeRaw(cmd, false);
    } else {
      await this.writeRaw(`${cmd} ${arg ? arg : ''}`, true);
    }

    await this.delay(XBEE_COMMAND_DELAY);

    let buffer = await this.readRaw();
    if (!buffer) {
      throw new Error(`XBee may require a restart; ${cmd} returned ${buffer}!`);
    }

    let ok = buffer.toString().trim();
    if (ok.indexOf('OK') === -1) {
      throw new Error(`Expected OK but got ${ok}`);
    }

    ok.replace('OK', '');
    console.log(ok);

    // Skip verification of certain commands
    if (['ATCN', 'ATRE', 'ATWR'].indexOf(cmd) !== -1) return;

    await this.verifyCommand(cmd, verifyResponse);
  }

  async verifyCommand(cmd, arg) {
    let cmdMode = false;
    if (cmd === '+++') {
      cmdMode = true;
    }

    if (cmdMode) {
      await this.writeRaw('AT', true);
    } else {
      await this.writeRaw(`${cmd}`, true);
    }

    await this.delay(XBEE_COMMAND_DELAY);
    let buffer = await this.readRaw();
    let response = buffer.toString().trim();
    if (response !== arg) {
      throw new Error(`Expected ${arg} but got ${response}`);
    }
  }
}

module.exports.XBee = XBee;

async function test() {
  const FrameBuilder = require('./frame/FrameBuilder').FrameBuilder;

  let dev = `/dev/ttyAMA0`;
  let xbee = new XBee(dev, 115200);

  let fb = new FrameBuilder();
  let buffer = fb.build(0x10, {
    destination64: '0013A200415B3CA7',
    destination16: 'fffe',
    data: '0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345'
  });

  await xbee.initialize();
  await xbee.enterCommandMode();
  await xbee.exitCommandMode();

  for (let i = 0; i < 10; i++) {
    await xbee.writeRaw(buffer.toString('hex'));
  }
}
