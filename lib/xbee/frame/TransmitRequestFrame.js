'use strict';

const Frame = require('./Frame').Frame;

class TransmitRequestFrame extends Frame {
  constructor() {
    super();
  }
}

module.exports.TransmitRequestFrame = TransmitRequestFrame;
