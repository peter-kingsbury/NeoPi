'use strict';

const fs       = require('fs');
const Bluebird = require('bluebird');
const gm       = Bluebird.promisifyAll(require('gm').subClass({ imageMagick: true }));
const path     = require('path');
const util     = require('util');
const readFileAsync = util.promisify(fs.readFile);
const openAsync = util.promisify(fs.open);
const writeAsync = util.promisify(fs.write);
const writeFileAsync = util.promisify(fs.writeFile);
const mkdirp = require('mkdirp-promise');

Bluebird.promisifyAll(gm.prototype);

class ImageProcessor {
  static async prepareForSending(file) {
    await gm(file)
      .noProfile()
      .bitdepth(8)
      .writeAsync(file);
  }

  static async imageToBuffers(file, bufferSize) {
    let contents = await readFileAsync(file);

    let buffers = [];
    let bufferCount = Math.ceil(contents.length / bufferSize);

    for (let i = 0; i < bufferCount; i++) {
      let buffer = Buffer.alloc(bufferSize, 0, 'binary');
      contents.copy(buffer, 0, i * bufferSize, i * bufferSize + bufferSize);
      // buffers.push(buffer.toString('hex'));
      buffers.push(buffer);
    }

    return buffers;
  }

  static async buffersToImage(buffers, file) {
    // let startLength = buffers.length;
    // let missing = 0;
    // for (let i = 0; i < buffers.length; i++) {
    //   let rnd = Math.random();
    //   if (rnd < 0.01) {
    //     buffers[i].fill(0);
    //     missing++;
    //   }
    // }
    //
    // console.log(`Rebuilding image with ${missing/startLength*100} % missing data`);


    let dirname = path.dirname(file);
    await mkdirp(dirname);

    let recomposed = Buffer.concat(buffers);
    await writeFileAsync(file, recomposed);
  }
}

module.exports.ImageProcessor = ImageProcessor;
