'use strict';

/**
 * The type used to indicate string lengths.
 * @type {string}
 */
const StringLengthType = 'uint16LE';

/**
 * The format used for strings.
 * @type {string}
 */
const StringEncoding = 'ascii';

/**
 * An enumeration used to map datatypes to their respective buffer pack and unpack functions.
 * @type {{doubleBE: {size: number, pack: string, unpack: string}, doubleLE: {size: number, pack: string, unpack: string}, floatBE: {size: number, pack: string, unpack: string}, floatLE: {size: number, pack: string, unpack: string}, int8: {size: number, pack: string, unpack: string}, int16BE: {size: number, pack: string, unpack: string}, int16LE: {size: number, pack: string, unpack: string}, int32BE: {size: number, pack: string, unpack: string}, int32LE: {size: number, pack: string, unpack: string}, intBE: {size: number, pack: string, unpack: string}, intLE: {size: number, pack: string, unpack: string}, uint8: {size: number, pack: string, unpack: string}, uint16BE: {size: number, pack: string, unpack: string}, uint16LE: {size: number, pack: string, unpack: string}, uint32BE: {size: number, pack: string, unpack: string}, uint32LE: {size: number, pack: string, unpack: string}, uintBE: {size: number, pack: string, unpack: string}, uintLE: {size: number, pack: string, unpack: string}, string: {size: number, pack: string, unpack: string}}}
 */
const DataTypeEnum = {
  doubleBE:  {size: 8, pack: 'writeDoubleBE', unpack: 'readDoubleBE'},
  doubleLE:  {size: 8, pack: 'writeDoubleLE', unpack: 'readDoubleLE'},
  floatBE:   {size: 4, pack: 'writeFloatBE',  unpack: 'readFloatBE'},
  floatLE:   {size: 4, pack: 'writeFloatLE',  unpack: 'readFloatLE'},
  int8:      {size: 1, pack: 'writeInt8',     unpack: 'readInt8'},
  int16BE:   {size: 2, pack: 'writeInt16BE',  unpack: 'readInt16BE'},
  int16LE:   {size: 2, pack: 'writeInt16LE',  unpack: 'readInt16LE'},
  int32BE:   {size: 4, pack: 'writeInt32BE',  unpack: 'readInt32BE'},
  int32LE:   {size: 4, pack: 'writeInt32LE',  unpack: 'readInt32LE'},
  intBE:     {size: 4, pack: 'writeIntBE',    unpack: 'readIntBE'},
  intLE:     {size: 4, pack: 'writeIntLE',    unpack: 'readIntLE'},
  uint8:     {size: 1, pack: 'writeUInt8',    unpack: 'readUInt8'},
  uint16BE:  {size: 2, pack: 'writeUInt16BE', unpack: 'readUInt16BE'},
  uint16LE:  {size: 2, pack: 'writeUInt16LE', unpack: 'readUInt16LE'},
  uint32BE:  {size: 4, pack: 'writeUInt32BE', unpack: 'readUInt32BE'},
  uint32LE:  {size: 4, pack: 'writeUInt32LE', unpack: 'readUInt32LE'},
  uintBE:    {size: 4, pack: 'writeUIntBE',   unpack: 'readUIntBE'},
  uintLE:    {size: 4, pack: 'writeUIntLE',   unpack: 'readUIntLE'},
  string:    {size: 0, pack: 'write',         unpack: 'read'}
};

/**
 * Pack class. Used for bidirectional object-to-buffer-to-object conversion.
 * Accepts any known Node.js v8 buffer data-types.
 *
 * Example Usage:

    var structure = [
      {key: 'a', type: 'uint8'},
      {key: 'b', type: 'int32BE'},
      {key: 'c', type: 'string'}
    ];

    var buffer = Pack.pack({
      a: 3,
      b: 557,
      c: 'hello, world!'
    }, structure);

    console.log(buffer);
    // <Buffer 03 00 00 02 2d 0d 00 68 65 6c 6c 6f 2c 20 77 6f 72 6c 64 21>

    console.log(buffer.toString('hex'));
    // 030000022d0d0068656c6c6f2c20776f726c6421

    var obj = Pack.unpack(buffer, structure);
    console.log(obj);
    // { a: 3, b: 557, c: 'hello, world!' }

 */

class Pack {
  /**
   * Given an object and structure, return the size of the buffer required to hold the object.
   * @param object The object to use as reference for calculating the buffer size.
   * @param structure An array containing 'key-type' pairs defining the type for each attribute in 'object'.
   * @returns {number} The size of the buffer required to hold the object, in bytes.
   * @private
   */
  static _calculateBufferSize(object, structure) {
    let size = 0;
    for (const entry of structure) {
      let type = entry.type;
      let key = entry.key;
      let value = object[key];
      let dte = DataTypeEnum[type];
      if (!dte) {
        throw new Error(`invalid type ${type}, must be one of ${Object.keys(DataTypeEnum).join(',')}`)
      }
      if (type === 'string') {
        size += DataTypeEnum[StringLengthType].size;
        size += value.length;
      } else {
        size += dte.size;
      }
    }

    return size;
  }

  /**
   * Packs a given object according to the given structure.
   * @param object The object to be packed.
   * @param structure The packing structure for the object's attributes.
   * @returns {*} A buffer containing the packed object.
   */
  static pack(object, structure) {
    let bufferSize = this._calculateBufferSize(object, structure);
    if (bufferSize <= 0) {
      throw new Error(`invalid size ${bufferSize}`);
    }

    // Use allocUnsafe, since we're overwriting each byte anyway.
    let buffer = Buffer.alloc(bufferSize, 0); // Buffer.allocUnsafe(bufferSize);
    let offset = 0;

    for (const entry of structure) {
      let type = entry.type;
      let key = entry.key;
      let value = object[key];
      let dte = DataTypeEnum[type];
      if (!dte) {
        throw new Error(`invalid type ${type}, must be one of ${Object.keys(DataTypeEnum).join(',')}`)
      }

      if (type === 'string') {
        let strlen = value.length;

        let stringLengthTypeSize = DataTypeEnum[StringLengthType].size;
        // Pack a string-length variable
        buffer[DataTypeEnum[StringLengthType].pack](strlen,  offset);
        offset += stringLengthTypeSize;

        // Then pack the actual string
        buffer[dte.pack](value, offset, strlen, StringEncoding);
        offset += strlen;
      } else {
        buffer[dte.pack](value, offset, dte.size);
        offset += dte.size;
      }

    }

    return buffer;
  }

  /**
   * Given a buffer and structure, unpack the buffer into an object.
   * @param buffer A previously-packed JS buffer.
   * @param structure The structure to be used for unpacking.
   * @returns {{}} The unpacked buffer as a JS object.
   */
  static unpack(buffer, structure) {
    let obj = {};
    let offset = 0;

    for (const entry of structure) {
      let type = entry.type;
      let key = entry.key;
      let dte = DataTypeEnum[type];
      if (!dte) {
        throw new Error(`invalid type ${type}, must be one of ${Object.keys(DataTypeEnum).join(',')}`)
      }
      let value = null;

      if (type === 'string') {
        // Unpack a string-length variable
        let stringLengthTypeSize = DataTypeEnum[StringLengthType].size;
        let strlen = buffer[DataTypeEnum[StringLengthType].unpack](offset, stringLengthTypeSize);
        offset += stringLengthTypeSize;
        // Unpack the actual string
        value = buffer.toString(StringEncoding, offset, offset + strlen);
        offset += strlen;
      } else {
        value = buffer[dte.unpack](offset, dte.size);
        offset += dte.size;
      }

      obj[key] = value;
    }

    return obj;
  }
}

module.exports.Pack = Pack;
