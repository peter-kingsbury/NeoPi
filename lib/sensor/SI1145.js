'use strict';

const Sensor = require('../core/Sensor').Sensor;
const Bluebird = require('bluebird');
const i2c = Bluebird.promisifyAll(require('i2c-bus'));

// COMMANDS
// const SI1145_PARAM_QUERY                      = 0x80;
const SI1145_PARAM_SET                        = 0xA0;
// const SI1145_NOP                              = 0x0;
const SI1145_RESET                            = 0x01;
// const SI1145_BUSADDR                          = 0x02;
// const SI1145_PS_FORCE                         = 0x05;
// const SI1145_ALS_FORCE                        = 0x06;
// const SI1145_PSALS_FORCE                      = 0x07;
// const SI1145_PS_PAUSE                         = 0x09;
// const SI1145_ALS_PAUSE                        = 0x0A;
// const SI1145_PSALS_PAUSE                      = 0xB;
// const SI1145_PS_AUTO                          = 0x0D;
// const SI1145_ALS_AUTO                         = 0x0E;
const SI1145_PSALS_AUTO                       = 0x0F;
// const SI1145_GET_CAL                          = 0x12;

// Parameters
// const SI1145_PARAM_I2CADDR                    = 0x00;
const SI1145_PARAM_CHLIST                     = 0x01;
const SI1145_PARAM_CHLIST_ENUV                = 0x80;
// const SI1145_PARAM_CHLIST_ENAUX               = 0x40;
const SI1145_PARAM_CHLIST_ENALSIR             = 0x20;
const SI1145_PARAM_CHLIST_ENALSVIS            = 0x10;
const SI1145_PARAM_CHLIST_ENPS1               = 0x01;
// const SI1145_PARAM_CHLIST_ENPS2               = 0x02;
// const SI1145_PARAM_CHLIST_ENPS3               = 0x04;

const SI1145_PARAM_PSLED12SEL                 = 0x02;
// const SI1145_PARAM_PSLED12SEL_PS2NONE         = 0x00;
// const SI1145_PARAM_PSLED12SEL_PS2LED1         = 0x10;
// const SI1145_PARAM_PSLED12SEL_PS2LED2         = 0x20;
// const SI1145_PARAM_PSLED12SEL_PS2LED3         = 0x40;
// const SI1145_PARAM_PSLED12SEL_PS1NONE         = 0x00;
const SI1145_PARAM_PSLED12SEL_PS1LED1         = 0x01;
// const SI1145_PARAM_PSLED12SEL_PS1LED2         = 0x02;
// const SI1145_PARAM_PSLED12SEL_PS1LED3         = 0x04;

// const SI1145_PARAM_PSLED3SEL                  = 0x03;
// const SI1145_PARAM_PSENCODE                   = 0x05;
// const SI1145_PARAM_ALSENCODE                  = 0x06;

const SI1145_PARAM_PS1ADCMUX                  = 0x07;
// const SI1145_PARAM_PS2ADCMUX                  = 0x08;
// const SI1145_PARAM_PS3ADCMUX                  = 0x09;
const SI1145_PARAM_PSADCOUNTER                = 0x0A;
const SI1145_PARAM_PSADCGAIN                  = 0x0B;
const SI1145_PARAM_PSADCMISC                  = 0x0C;
const SI1145_PARAM_PSADCMISC_RANGE            = 0x20;
const SI1145_PARAM_PSADCMISC_PSMODE           = 0x04;

const SI1145_PARAM_ALSIRADCMUX                = 0x0E;
// const SI1145_PARAM_AUXADCMUX                  = 0x0F;

const SI1145_PARAM_ALSVISADCOUNTER            = 0x10;
const SI1145_PARAM_ALSVISADCGAIN              = 0x11;
const SI1145_PARAM_ALSVISADCMISC              = 0x12;
const SI1145_PARAM_ALSVISADCMISC_VISRANGE     = 0x20;

const SI1145_PARAM_ALSIRADCOUNTER             = 0x1D;
const SI1145_PARAM_ALSIRADCGAIN               = 0x1E;
const SI1145_PARAM_ALSIRADCMISC               = 0x1F;
const SI1145_PARAM_ALSIRADCMISC_RANGE         = 0x20;

const SI1145_PARAM_ADCCOUNTER_511CLK          = 0x70;

const SI1145_PARAM_ADCMUX_SMALLIR             = 0x00;
const SI1145_PARAM_ADCMUX_LARGEIR             = 0x03;

// REGISTERS
// const SI1145_REG_PARTID                       = 0x00;
// const SI1145_REG_REVID                        = 0x01;
// const SI1145_REG_SEQID                        = 0x02;

const SI1145_REG_INTCFG                       = 0x03;
const SI1145_REG_INTCFG_INTOE                 = 0x01;
// const SI1145_REG_INTCFG_INTMODE               = 0x02;

const SI1145_REG_IRQEN                        = 0x04;
const SI1145_REG_IRQEN_ALSEVERYSAMPLE         = 0x01;
// const SI1145_REG_IRQEN_PS1EVERYSAMPLE         = 0x04;
// const SI1145_REG_IRQEN_PS2EVERYSAMPLE         = 0x08;
// const SI1145_REG_IRQEN_PS3EVERYSAMPLE         = 0x10;

const SI1145_REG_IRQMODE1                     = 0x05;
const SI1145_REG_IRQMODE2                     = 0x06;

// const SI1145_REG_HWKEY                        = 0x07;
const SI1145_REG_MEASRATE0                    = 0x08;
const SI1145_REG_MEASRATE1                    = 0x09;
// const SI1145_REG_PSRATE                       = 0x0A;
const SI1145_REG_PSLED21                      = 0x0F;
// const SI1145_REG_PSLED3                       = 0x10;
const SI1145_REG_UCOEFF0                      = 0x13;
const SI1145_REG_UCOEFF1                      = 0x14;
const SI1145_REG_UCOEFF2                      = 0x15;
const SI1145_REG_UCOEFF3                      = 0x16;
const SI1145_REG_PARAMWR                      = 0x17;
const SI1145_REG_COMMAND                      = 0x18;
// const SI1145_REG_RESPONSE                     = 0x20;
const SI1145_REG_IRQSTAT                      = 0x21;
// const SI1145_REG_IRQSTAT_ALS                  = 0x01;

const SI1145_REG_ALSVISDATA0                  = 0x22;
// const SI1145_REG_ALSVISDATA1                  = 0x23;
const SI1145_REG_ALSIRDATA0                   = 0x24;
// const SI1145_REG_ALSIRDATA1                   = 0x25;
const SI1145_REG_PS1DATA0                     = 0x26;
// const SI1145_REG_PS1DATA1                     = 0x27;
// const SI1145_REG_PS2DATA0                     = 0x28;
// const SI1145_REG_PS2DATA1                     = 0x29;
// const SI1145_REG_PS3DATA0                     = 0x2A;
// const SI1145_REG_PS3DATA1                     = 0x2B;
const SI1145_REG_UVINDEX0                     = 0x2C;
// const SI1145_REG_UVINDEX1                     = 0x2D;
const SI1145_REG_PARAMRD                      = 0x2E;
// const SI1145_REG_CHIPSTAT                     = 0x30;

// I2C Address
const SI1145_ADDR                             = 0x60;

class SI1145 extends Sensor {
  constructor(options) {
    super(options);
    this.name = 'SI1145';
    this.address = SI1145_ADDR;
    this._wire = Bluebird.promisifyAll(i2c.openSync(1));
  }

  _reset() {
    return Promise.all([
      this._wire.writeByteAsync(this.address, SI1145_REG_MEASRATE0, 0),
      this._wire.writeByteAsync(this.address, SI1145_REG_MEASRATE1, 0),
      this._wire.writeByteAsync(this.address, SI1145_REG_IRQEN,     0),
      this._wire.writeByteAsync(this.address, SI1145_REG_IRQMODE1,  0),
      this._wire.writeByteAsync(this.address, SI1145_REG_IRQMODE2,  0),
      this._wire.writeByteAsync(this.address, SI1145_REG_INTCFG,    0),
      this._wire.writeByteAsync(this.address, SI1145_REG_IRQSTAT,   0xFF),
      this._wire.writeByteAsync(this.address, SI1145_REG_COMMAND,   SI1145_RESET),
      // Bluebird.delay(100)
      // this._wire.writeByteAsync(this.address, SI1145_REG_HWKEY,     0x17),
      Bluebird.delay(100)
    ]).catch(err => {
      console.trace(err);
    });
  }

  _init() {
    return Promise.all([
      this._reset(),
      // Enable UVindex measurement coefficients
      this._wire.writeByteAsync(this.address, SI1145_REG_UCOEFF0, 0x29),
      this._wire.writeByteAsync(this.address, SI1145_REG_UCOEFF1, 0x89),
      this._wire.writeByteAsync(this.address, SI1145_REG_UCOEFF2, 0x02),
      this._wire.writeByteAsync(this.address, SI1145_REG_UCOEFF3, 0x00),
      // Enable UV sensor
      this._writeParam(SI1145_PARAM_CHLIST, SI1145_PARAM_CHLIST_ENUV | SI1145_PARAM_CHLIST_ENALSIR | SI1145_PARAM_CHLIST_ENALSVIS | SI1145_PARAM_CHLIST_ENPS1),
      // Enable interrupt on every sample
      this._wire.writeByteAsync(this.address, SI1145_REG_INTCFG, SI1145_REG_INTCFG_INTOE),
      this._wire.writeByteAsync(this.address, SI1145_REG_IRQEN, SI1145_REG_IRQEN_ALSEVERYSAMPLE),
      // Program LED current
      this._wire.writeByteAsync(this.address, SI1145_REG_PSLED21, 0x03),
      this._writeParam(SI1145_PARAM_PS1ADCMUX, SI1145_PARAM_ADCMUX_LARGEIR),
      // Prox sensor #1 uses LED #1
      this._writeParam(SI1145_PARAM_PSLED12SEL, SI1145_PARAM_PSLED12SEL_PS1LED1),
      // Fastest clocks, clock div 1
      this._writeParam(SI1145_PARAM_PSADCGAIN, 0),
      // Take 511 clocks to measure
      this._writeParam(SI1145_PARAM_PSADCOUNTER, SI1145_PARAM_ADCCOUNTER_511CLK),
      // in prox mode, high range
      this._writeParam(SI1145_PARAM_PSADCMISC, SI1145_PARAM_PSADCMISC_RANGE | SI1145_PARAM_PSADCMISC_PSMODE),
      this._writeParam(SI1145_PARAM_ALSIRADCMUX, SI1145_PARAM_ADCMUX_SMALLIR),
      // Fastest clocks, clock div 1
      this._writeParam(SI1145_PARAM_ALSIRADCGAIN, 0),
      // Take 511 clocks to measure
      this._writeParam(SI1145_PARAM_ALSIRADCOUNTER, SI1145_PARAM_ADCCOUNTER_511CLK),
      // in high range mode
      this._writeParam(SI1145_PARAM_ALSIRADCMISC, SI1145_PARAM_ALSIRADCMISC_RANGE),
      // fastest clocks, clock div 1
      this._writeParam(SI1145_PARAM_ALSVISADCGAIN, 0),
      // Take 511 clocks to measure
      this._writeParam(SI1145_PARAM_ALSVISADCOUNTER, SI1145_PARAM_ADCCOUNTER_511CLK),
      // in high range mode (not normal signal)
      this._writeParam(SI1145_PARAM_ALSVISADCMISC, SI1145_PARAM_ALSVISADCMISC_VISRANGE),
      // measurement rate for auto
      this._wire.writeByteAsync(this.address, SI1145_REG_MEASRATE0, 0xFF), // 255 * 31.25uS = 8ms
      // auto run
      this._wire.writeByteAsync(this.address, SI1145_REG_COMMAND, SI1145_PSALS_AUTO)
    ]).catch(err => {
      console.trace(err);
    });
  }

  _writeParam(p, v) {
    return Promise.all([
      this._wire.writeByteAsync(this.address, SI1145_REG_PARAMWR, v),
      this._wire.writeByteAsync(this.address, SI1145_REG_COMMAND, p | SI1145_PARAM_SET),
      this._wire.readByteAsync(this.address, SI1145_REG_PARAMRD)
    ]).then(result => {
      return result.splice(-1).pop();
    }).catch(err => {
      console.trace(err);
    });
  }

  readUV() {
    return Promise.all([
      this._wire.readWordAsync(this.address, SI1145_REG_UVINDEX0).then(result => {
        return result;
      })
    ]).then(result => {
      return result.splice(-1).pop();
    });
  }

  readVisible() {
    return Promise.all([
      this._wire.readWordAsync(this.address, SI1145_REG_ALSVISDATA0)
    ]).then(result => {
      return result.splice(-1).pop();
    });
  }

  readIR() {
    return Promise.all([
      this._wire.readWordAsync(this.address, SI1145_REG_ALSIRDATA0)
    ]).then(result => {
      return result.splice(-1).pop();
    });
  }

  readProx() {
    return Promise.all([
      this._wire.readWordAsync(this.address, SI1145_REG_PS1DATA0)
    ]).then(result => {
      return result.splice(-1).pop();
    });
  }
}

module.exports.SI1145 = SI1145;

