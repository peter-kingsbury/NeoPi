'use strict';

const Sensor = require('../core/Sensor').Sensor;
const tsl2591 = require('tsl2591');
const Bluebird = require('bluebird');

const ALS_GAIN = 0;
const ALS_DURATION = 0;

class TSL2591 extends Sensor {
  constructor(options) {
    super(options);
    this.address = 0x29;
    this.name = 'TSL2591';
    this._sensor = Bluebird.promisifyAll(new tsl2591({device: '/dev/i2c-1'}));
  }

  /**
   * Initialize the library.
   * @param options
   * @returns {*}
   * @private
   */
  _init(options = {}) {
    let gain = options.gain || ALS_GAIN;
    let duration = options.duration || ALS_DURATION;
    return this._sensor.initAsync({ AGAIN: gain, ATIME: duration });
  }

  /**
   * Read the sensor's luminosity data, ranging from 0.144 Lux to 80,000 Lux.
   * @returns {Promise.<T>}
   */
  readLuminosity() {
    return Promise.all([
      //Bluebird.delay(250),
      this._sensor.readLuminosityAsync()
    ]).then(result => {
      return result.splice(-1).pop();
    });
  }

}

module.exports.TSL2591 = TSL2591;

