'use strict';

const Sensor = require('../core/Sensor').Sensor;
const bme280 = require('bme280-sensor');

class BME280 extends Sensor {
  constructor(options) {
    super(options);
    this.address = 0x77;
    this.name = 'BME280';
    this._sensor = new bme280({
      i2cBusNo   : 1, // defaults to 1
      i2cAddress : bme280.BME280_DEFAULT_I2C_ADDRESS() // defaults to 0x77
    });
    this.seaLevel = 0;
  }

  _init() {
    return this._sensor.reset().then(() => {
      return this._sensor.init().catch(err => {
        console.trace(err);
        throw err;
      });
    });
  }

  read() {
    return this._sensor.readSensorData().then(result => {
      result.altitude = bme280.calculateAltitudeMeters(result.pressure_hPa, this.seaLevel);
      return result;
    });
  }
}

module.exports.BME280 = BME280;
