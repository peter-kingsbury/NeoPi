'use strict';

const path     = require('path');
const RaspiCam = require('raspicam');
const Moment   = require('moment');

const { ImageProcessor } = require('../../lib/image/ImageProcessor');

const MODE     = 'photo';
const WIDTH    = 3280;
const HEIGHT   = 2464;
const QUALITY  = 100;
const ROTATION = 0;
const TIMEOUT  = 5000;
const VSTAB    = true;
const SHARPNESS = 100;
const ENCODING = 'jpg';

class Camera {
  constructor(options) {
    this._output = options.output;
    this._width = options.width || WIDTH;
    this._height = options.height || HEIGHT;
    this._quality = options.quality || QUALITY;
    this._rotation = options.rotation || ROTATION;
    this._encoding = options.encoding || 'jpg';
  }

  capture(options = {}) {
    return new Promise((resolve, reject) => {
      try {
        let now = Moment().format('YYYYMMDD-HHmmss.SSS');
        let filename = `image-${now}.${options.encoding || ENCODING}`;
        let filepath = path.resolve(path.join(this._output, filename));
        let opts = {
          mode:      MODE,
          output:    filepath,
          width:     options.width || WIDTH,
          height:    options.height || HEIGHT,
          quality:   options.quality || QUALITY,
          rotation:  options.rotation || ROTATION,
          timeout:   options.timeout || TIMEOUT,
          vstab:     options.vstab || VSTAB,
          sharpness: options.sharpness || SHARPNESS,
          encoding:  options.encoding || ENCODING,
          nopreview: true,
          log:       options.log ? options.log : () => {}
        };
        let camera = new RaspiCam(opts);

        camera.on('start', () => {
          console.log(`[RASPICAM] Start capture to ${filepath}...`);
        });

        camera.on('read', () => {
          // console.log(`[RASPICAM] Reading...`);
        });

        camera.on('stop', () => {
          console.log(`[RASPICAM] Stopped.`);
        });

        camera.on('exit', () => {
          console.log(`[RASPICAM] Exited.`);
          resolve(filepath);
        });

        camera.start();
      } catch (e) {
        reject(e);
      }
    });
  }

  capture_v1() {
    return new Promise((resolve, reject) => {
      try {
        let now = Moment().format('YYYYMMDD-HHmmss.SSS');
        let filename = `image-${now}.${this._encoding}`;
        let filepath = path.resolve(path.join(this._output, filename));
        let opts = {
          mode:      MODE,
          output:    filepath,
          width:     this._width,
          height:    this._height,
          quality:   this._quality,
          rotation:  this._rotation,
          timeout:   100,
          vstab:     true,
          sharpness: 100,
          encoding:  this._encoding,
          nopreview: true,
          log:       () => {}
        };
        let camera = new RaspiCam(opts);

        camera.on('start', () => {
          console.log(`[RASPICAM] Start capture to ${filepath}...`);
        });

        camera.on('read', () => {
          // console.log(`[RASPICAM] Reading...`);
        });

        camera.on('stop', () => {
          console.log(`[RASPICAM] Stopped.`);
        });

        camera.on('exit', () => {
          console.log(`[RASPICAM] Exited.`);
          resolve(filepath);
        });

        camera.start();
      } catch (e) {
        reject(e);
      }
    });
  }
}

module.exports.Camera = Camera;

/*
 var piexif = require("piexif.js");
 var fs = required("fs");

 var jpeg = fs.readFileSync(filename1);
 var data = jpeg.toString("binary");
 var exifObj = piexif.load(data);
 exifObj["GPS"][piexif.GPSIFD.GPSVersionID] = [7, 7, 7, 7];
 exifObj["GPS"][piexif.GPSIFD.GPSDateStamp] = "1999:99:99 99:99:99";
 var exifbytes = piexif.dump(exifObj);
 var newData = piexif.insert(exifbytes, data);
 var newJpeg = new Buffer(newData, "binary");
 fs.writeFileSync(filename2, newJpeg);
 */


(async () => {
  /**
  * Test 1:
  * - take a JPG image at full resolution
  * - resize the resulting image to 320x240
  * - save as a gif
  * */

 /* try {
    let encoding = 'jpg';
    let start = Date.now();
    let cam = new Camera({
      // width: 320,
      // height: 240,
      output: `tmp`,
      rotation: 90,
      encoding
    });

    let file = await cam.capture();
    let end = Date.now();
    console.log(`${encoding} total time: ${end - start} ms`);

    start = Date.now();
    gm(file)
      .resize(320, 240, '!')
      .noProfile()
      .bitdepth(8)
     // .type('Grayscale')
      .interlace('Line')
      .write(file.replace(encoding, 'gif'), () => {
        end = Date.now();
        console.log(`${encoding} -> gif total time: ${end - start} ms`);
      });

  } catch (e) {
    console.trace(e);
  }*/

  /**
   * Test 2:
   * - take a JPG image at full resolution
   * - take a second GIF image at 320x240
   */

  return;

  try {
    let start;
    let end;

    start = Date.now();
    await (new Camera({
      width: 320,
      height: 240,
      output: `tmp`,
      rotation: 90,
      encoding: 'jpg'
    })).capture();
    end = Date.now();
    console.log(`jpg total time: ${end - start} ms`);

    start = Date.now();
    let encoding = 'jpg';
    let sendFile = await (new Camera({
      width: 320,
      height: 240,
      output: `tmp`,
      rotation: 90,
      encoding
    })).capture();

    await ImageProcessor.prepareForSending(sendFile);

    end = Date.now();
    console.log(`jpg total time: ${end - start} ms`);

    let buffers = await ImageProcessor.imageToBuffers(sendFile, 100);

    await ImageProcessor.buffersToImage(buffers, `./tmp/recompose.${encoding}`);

    console.log(buffers.length);
  } catch (e) {
    console.trace(e);
  }
})();
