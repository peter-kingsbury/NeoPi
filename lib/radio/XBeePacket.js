'use strict';

const XBeePacketType = {};

const XBeePacketFormat = {

};

/**
 *
 */
class XBeePacket {
  /**
   *
   */
  constructor() {
    this._start = 0x45;
    this._size = 0;
    this._type = 0;
    this._timestamp = 0;
    this._series = 0;
    this._index = 0;
    this._count = 0;
    this._format = 0;
    this._buffer = null;
    this._crc = null;
  }

  /**
   *
   * @private
   */
  _calculateChecksum() {
    let data = this.crc;
  }

  /**
   *
   * @returns {*}
   */
  get crc() {
    return this._calculateChecksum();
  }

  /**
   *
   * @returns {{type: number, timestamp: (number|*), series: number, index: number, count: number, format: number, buffer: null}}
   */
  get crcData() {
    return {
      type:      this._type,
      timestamp: this._timestamp,
      series:    this._series,
      index:     this._index,
      count:     this._count,
      format:    this._format,
      buffer:    this._buffer
    };
  }

  /**
   *
   * @returns {{start: number, size: number, type: number, timestamp: (number|*), series: number, index: number, count: number, format: number, buffer: null, crc: *}}
   */
  get packData() {
    return {
      start:     this._start,
      size:      this._size,
      type:      this._type,
      timestamp: this._timestamp,
      series:    this._series,
      index:     this._index,
      count:     this._count,
      format:    this._format,
      buffer:    this._buffer,
      crc:       this.crc
    };
  }

  /**
   *
   * @param value
   */
  set crc(value) {
    throw new Error('read only');
  }

  /**
   *
   * @param data
   * @param type
   */
  pack(data, type) {
    switch (type) {
      case 'uint8': {
      } break;
    }
  }
}

module.exports.XBeePacket = XBeePacket;
