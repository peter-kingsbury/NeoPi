'use strict';

class PacketSequence {
  constructor(options) {
    if (!options.sequenceLength) {
      throw new Error('need the sequenceLength');
    }

    this._packets = [];
    this._sequenceLength = parseInt(options.sequenceLength);
    this._createdAt = new Date();
  }

  addPacket(sequence, packet) {
    this._packets[sequence] = packet;
  }

  isComplete() {
    return this._packets.length === this._sequenceLength;
  }
}

module.exports.PacketSequence = PacketSequence;
