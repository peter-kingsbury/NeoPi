'use strict';

const PACKET_ID = {
  0: 'system',
  1: 'heartbeat',
  2: 'bme280',
  3: 'ds18B20',
  4: 'gps',
  5: 'hmc5883l',
  6: 'lis3dh',
  7: 'si1145',
  8: 'tsl2591',
  20: 'sensor_ccc',
  21: 'image_ccc',

  SYSTEM:         0,
  HEARTBEAT:      1,
  ENVIRONMENT:    2,
  TEMPERATURE:    3,
  GPS:            4,
  COMPASS:        5,
  ACCELEROMETER:  6,
  ULTRAVIOLET:    7,
  LUMINOSITY:     8,
  ENVIRONMENT2:   9,
  ENVIRONMENT3:   10,

  SENSOR_CCC:     20,
  IMAGE_CCC:      21,
  IMAGECHUNK_CCC: 88,

  IMAGECHUNK:     100
};

const BYTE_SIZE = {
  CHAR: 1,
  SHORT: 2,
  LONG: 4,
  FLOAT: 4,
  DOUBLE: 8
};

const DATA_TYPE = {
  FLOAT32: 'float32',
  DOUBLE: 'double',
  UINT8: 'uint8',
  UINT16: 'uint16',
  UINT32: 'uint32',
  INT8: 'int8',
  INT16: 'int16',
  INT32: 'int32',
  STRING: 'string',
  INTP1: 'intp1',
  INTP2: 'intp2',
  INTP3: 'intp3',
  INTP4: 'intp4',
  INTP5: 'intp5',
  INTP6: 'intp6',
  BUFFER_88: 'buffer88',
  BUFFER_100: 'buffer100'
};

/**
 * @class Packet
 */
class Packet {
  constructor() {
    this.templates = {
      [PACKET_ID.SYSTEM]:        `cpu: ${DATA_TYPE.FLOAT32}, memory: ${DATA_TYPE.FLOAT32}, disk: ${DATA_TYPE.FLOAT32}`,
      [PACKET_ID.HEARTBEAT]:     `timestamp: ${DATA_TYPE.INT32}, time: ${DATA_TYPE.INT32}`,
      [PACKET_ID.ENVIRONMENT]:   `temperature: ${DATA_TYPE.FLOAT32}, humidity: ${DATA_TYPE.FLOAT32}, pressure: ${DATA_TYPE.FLOAT32}, altitude: ${DATA_TYPE.FLOAT32}`,
      [PACKET_ID.TEMPERATURE]:   `temperature: ${DATA_TYPE.FLOAT32}`,
      [PACKET_ID.GPS]:           `latitude: ${DATA_TYPE.FLOAT32}, longitude: ${DATA_TYPE.FLOAT32}, speed: ${DATA_TYPE.FLOAT32}, altitude: ${DATA_TYPE.FLOAT32}, angle: ${DATA_TYPE.FLOAT32}`,
      [PACKET_ID.COMPASS]:       `heading: ${DATA_TYPE.FLOAT32}, x: ${DATA_TYPE.FLOAT32}, y: ${DATA_TYPE.FLOAT32}, z: ${DATA_TYPE.FLOAT32}`,
      [PACKET_ID.ACCELEROMETER]: `accelX: ${DATA_TYPE.FLOAT32}, accelY: ${DATA_TYPE.FLOAT32}, accelZ: ${DATA_TYPE.FLOAT32}`,
      [PACKET_ID.ULTRAVIOLET]:   `uv: ${DATA_TYPE.FLOAT32}`,
      [PACKET_ID.LUMINOSITY]:    `visibility: ${DATA_TYPE.FLOAT32}, ir: ${DATA_TYPE.FLOAT32}`,
      [PACKET_ID.IMAGECHUNK]:    `sequence: ${DATA_TYPE.UINT16}, count: ${DATA_TYPE.UINT16}, index: ${DATA_TYPE.UINT16}, data: ${DATA_TYPE.BUFFER_100}`

      [PACKET_ID.SENSOR_CCC]:     `type: ${DATA_TYPE.UINT8}, proximityFlag: ${DATA_TYPE.UINT8}, gps_nsats: ${DATA_TYPE.UINT8}, gps_status: ${DATA_TYPE.UINT8}, gps_mode: ${DATA_TYPE.UINT8}, gps_lat: ${DATA_TYPE.FLOAT32}, gps_lon: ${DATA_TYPE.FLOAT32}, gps_alt: ${DATA_TYPE.FLOAT32}, gps_gspd: ${DATA_TYPE.FLOAT32}, gps_dir: ${DATA_TYPE.FLOAT32}, gps_vspd: ${DATA_TYPE.FLOAT32}, ahrs_head: ${DATA_TYPE.FLOAT32}, ahrs_pitch: ${DATA_TYPE.FLOAT32}, ahrs_roll: ${DATA_TYPE.FLOAT32}, mpl_temp: ${DATA_TYPE.FLOAT32}, mpl_pres: ${DATA_TYPE.FLOAT32}, mpl_alt: ${DATA_TYPE.FLOAT32}, dht_temp: ${DATA_TYPE.FLOAT32}, dht_relh: ${DATA_TYPE.FLOAT32}, bat_rpi: ${DATA_TYPE.FLOAT32}, bat_ard: ${DATA_TYPE.FLOAT32}`,
      [PACKET_ID.IMAGECHUNK_CCC]: `type: ${DATA_TYPE.UINT8}, img_chunksize: ${DATA_TYPE.UINT8}, img_id: ${DATA_TYPE.UINT16}, img_chunk_id: ${DATA_TYPE.UINT16}, img_nchunks: ${DATA_TYPE.UINT16}, img_w: ${DATA_TYPE.UINT16}, img_h: ${DATA_TYPE.UINT16}, img_chunk: ${DATA_TYPE.BUFFER_88}`
    };
  }

  configure(templates) {
    this.templates = templates;
  }

  /**
   * Given an object and related template, serialize the data into a binary packet.
   * @param {Object} object The object to be serialized.
   * @param {Number} templateId The template descriptor.
   * @return {Buffer} A buffer containing serialized data.
   */
  serialize(object, templateId) {
    let id = -1;
    try {
      if (!this.templates.hasOwnProperty(templateId)) {
        throw new Error(`invalid templateId ${templateId}`);
      }

      let t = this.templates[templateId].replace(/\s/g, '');
      let data = [];

      id = Buffer.alloc(1, 0, 'binary');
      id.writeUInt8(templateId);
      data.push(id);

      let items = t.split(',');
      for (const entry of items) {
        let parts = /(\w+):(\w+)/g.exec(entry);
        let attribute = parts[1];
        let type = parts[2];
        switch (type) {
          case DATA_TYPE.FLOAT32: {
            let buf = Buffer.alloc(BYTE_SIZE.FLOAT, 0, 'binary');
            buf.writeFloatBE(object[attribute]);
            data.push(buf);
          } break;

          case DATA_TYPE.UINT8: {
            let buf = Buffer.alloc(BYTE_SIZE.CHAR, 0, 'binary');
            buf.writeUInt8(object[attribute]);
            data.push(buf);
          } break;

          case DATA_TYPE.UINT16: {
            let buf = Buffer.alloc(BYTE_SIZE.SHORT, 0, 'binary');
            buf.writeUInt16BE(object[attribute]);
            data.push(buf);
          } break;

          case DATA_TYPE.DOUBLE: {
            let buf = Buffer.alloc(BYTE_SIZE.DOUBLE, 0, 'binary');
            buf.writeDoubleBE(object[attribute]);
            data.push(buf);
          } break;

          case DATA_TYPE.INTP6: {
            let buf = Buffer.alloc(BYTE_SIZE.LONG, 0, 'binary');
            buf.writeInt32BE(parseInt(object[attribute] * Math.pow(10, 6)));
            data.push(buf);
          } break;

          case DATA_TYPE.BUFFER_100: {
            let buf = Buffer.from(object[attribute], 'hex');
            data.push(buf);
          } break;
        }
      }

      return Buffer.concat(data);
    } catch (e) {
      console.log(id);
      console.trace(e);
      throw e;
    }
  }

  /**
   * Given a binary buffer object, deserialize the buffer into a JavaScript object.
   * @param {Object} buffer The buffer to be deserialized.
   * @return {Object} A deserialized JavaScript object.
   */
  deserialize(buffer) {
    let id = -1;
    try {
      let offset = 0;
      id = buffer.readUInt8(offset);

      if (!this.templates.hasOwnProperty(id)) {
        throw new Error(`invalid templateId ${id}`);
      }

      let t = this.templates[id].replace(/\s/g, '');
      let items = t.split(',');
      let result = {};

      offset++;

      for (const entry of items) {
        let parts = /(\w+):(\w+)/g.exec(entry);
        let attribute = parts[1];
        let type = parts[2];
        switch (type) {
          case DATA_TYPE.FLOAT32: {
            Object.assign(result, {
              [attribute]: buffer.readFloatBE(offset)
            });
            offset += BYTE_SIZE.FLOAT;
          } break;

          case DATA_TYPE.UINT8: {
            Object.assign(result, {
              [attribute]: buffer.readUInt8(offset)
            });
            offset += BYTE_SIZE.CHAR;
          } break;

          case DATA_TYPE.UINT16: {
            Object.assign(result, {
              [attribute]: buffer.readUInt16BE(offset)
            });
            offset += BYTE_SIZE.SHORT;
          } break;

          case DATA_TYPE.DOUBLE: {
            Object.assign(result, {
              [attribute]: buffer.readDoubleBE(offset)
            });
            offset += BYTE_SIZE.DOUBLE;
          } break;

          case DATA_TYPE.INTP6: {
            Object.assign(result, {
              [attribute]: parseFloat(buffer.readInt32BE(offset) * Math.pow(10, -6))
            });
            offset += BYTE_SIZE.LONG;
          } break;

          case DATA_TYPE.BUFFER_88: {
            let buf = Buffer.alloc(88);
            buffer.copy(buf, 0, offset, offset + 88)

            Object.assign(result, {
              [attribute]: buf
            });

            offset += 88;
          } break;

          case DATA_TYPE.BUFFER_100: {
            let buf = Buffer.alloc(100);
            buffer.copy(buf, 0, offset, offset + 100)

            Object.assign(result, {
              [attribute]: buf
            });

            offset += 100;
          } break;
        }
      }

      return {
        packetId: id,
        data: result
      };
    } catch (e) {
      console.log(id);
      console.trace(e);
      throw e;
    }
  }
}

module.exports = {
  Packet,
  PACKET_ID,
  DATA_TYPE,
  BYTE_SIZE
};
