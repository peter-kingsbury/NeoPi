'use strict';

const { Packet, PACKET_ID, DATA_TYPE } = require('../lib/packet/Packet');

exports['default'] = {
  packet: (api) => {
    return {
      [PACKET_ID.ENVIRONMENT]: `temperature: ${DATA_TYPE.FLOAT}, humidity: ${DATA_TYPE.FLOAT}, pressure: ${DATA_TYPE.FLOAT}, altitude: ${DATA_TYPE.FLOAT}`,
      [PACKET_ID.TEMPERATURE]: `temperature: ${DATA_TYPE.FLOAT}`,
      [PACKET_ID.GPS]: `latitude: ${DATA_TYPE.FLOAT}, longitude: ${DATA_TYPE.FLOAT}, speed: ${DATA_TYPE.FLOAT}, altitude: ${DATA_TYPE.FLOAT}, angle: ${DATA_TYPE.FLOAT}`,
      [PACKET_ID.COMPASS]: `heading: ${DATA_TYPE.FLOAT}, x: ${DATA_TYPE.FLOAT}, y: ${DATA_TYPE.FLOAT}, z: ${DATA_TYPE.FLOAT}`,
      [PACKET_ID.ACCELEROMETER]: `accelX: ${DATA_TYPE.FLOAT}, accelY: ${DATA_TYPE.FLOAT}, accelZ: ${DATA_TYPE.FLOAT}`,
      [PACKET_ID.ULTRAVIOLET]: `uv: ${DATA_TYPE.FLOAT}`,
      [PACKET_ID.LUMINOSITY]: `visibility: ${DATA_TYPE.FLOAT}, ir: ${DATA_TYPE.FLOAT}`
    };
  }
};
