'use strict';

exports['default'] = {
  camera: (api) => {
    if (process.env.MODE === 'MISSIONCONTROL') return null;

    return {
      photoOutputDir: './data/images',
      cameraRotation: 90,
      vgaPalette: './data/vga.png'
    };
  }
};
