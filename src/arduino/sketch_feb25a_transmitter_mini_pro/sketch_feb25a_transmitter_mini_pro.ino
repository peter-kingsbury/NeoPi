#include <SPI.h>
#include <RH_RF95.h>
#include <QueueList.h>
#include <MemoryFree.h>
#include <Wire.h>

#define RFM95_CS 10
#define RFM95_RST 9
#define RFM95_INT 2
#define RF95_FREQ 915.0
#define LED_RX 5
#define LED_TX 6
#define BLINK_DELAY 1
#define SLAVE_ADDRESS 0x04
#define RADIO_WAIT_TIMEOUT 1000
#define MAX_PACKET 100

// Singleton instance of the radio driver
RH_RF95 rf95(RFM95_CS, RFM95_INT);

typedef struct PACKET {
  char Text[MAX_PACKET];
  uint8_t Length;
} Packet;

QueueList<Packet> PacketQueue;

char ReceiveBuffer[MAX_PACKET] = "\0";
int ReceiveBufferIndex = 0;

void i2cInit() {
  // I2C
  Wire.begin(SLAVE_ADDRESS);
  Wire.onReceive(onI2CReceiveData);
  Wire.onRequest(onI2CSendData);
}

void radioInit() {
  // RADIO
  pinMode(RFM95_RST, OUTPUT);
  digitalWrite(RFM95_RST, HIGH);

  //while (!Serial);
  Serial.begin(115200);
  delay(100);

  Serial.println("RFM9x Transmitter");

  // manual reset
  digitalWrite(RFM95_RST, LOW);
  delay(10);
  digitalWrite(RFM95_RST, HIGH);
  delay(10);

  while (!rf95.init()) {
    Serial.println("Failed to initialize LoRA radio unit");
    while (1);
  }
  Serial.println("Initialized LoRA radio unit");

  // Defaults after init are 434.0MHz, modulation GFSK_Rb250Fd250, +13dbM
  if (!rf95.setFrequency(RF95_FREQ)) {
    Serial.println("setFrequency failed");
    while (1);
  }
  Serial.print("Frequency set to ");
  Serial.print(RF95_FREQ);
  Serial.println(" MHz");

  // Defaults after init are 434.0MHz, 13dBm, Bw = 125 kHz, Cr = 4/5, Sf = 128chips/symbol, CRC on

  // The default transmitter power is 13dBm, using PA_BOOST.
  // If you are using RFM95/96/97/98 modules which uses the PA_BOOST transmitter pin, then
  // you can set transmitter powers from 5 to 23 dBm:
  rf95.setTxPower(23, false);
}

void setup() {
  pinMode(LED_RX, OUTPUT);
  pinMode(LED_TX, OUTPUT);
  i2cInit();
  radioInit();
}

/**
   Send one packet via radio broadcast.
*/
void radioSendPacket(Packet& packet) {
  Serial.print("SEND: ");
  Serial.print(packet.Text);

  digitalWrite(LED_TX, HIGH);
  delay(BLINK_DELAY);
  rf95.send((uint8_t *)packet.Text, packet.Length);
  rf95.waitPacketSent();
  digitalWrite(LED_TX, LOW);

  // Wait for a reply
  uint8_t buf[RH_RF95_MAX_MESSAGE_LEN];
  uint8_t len = sizeof(buf);

  if (rf95.waitAvailableTimeout(RADIO_WAIT_TIMEOUT)) {
    digitalWrite(LED_RX, HIGH);
    delay(BLINK_DELAY);

    if (rf95.recv(buf, &len)) {
      Serial.print(" ACK (RSS=");
      Serial.print(rf95.lastRssi(), DEC);
      Serial.print(")");
    } else {
      Serial.print("Receive failed");
    }
    digitalWrite(LED_RX, LOW);
  } else {
    Serial.print(" NACK");
  }

  // Display free memory
  Serial.print(" (");
  Serial.print(freeMemory(), DEC);
  Serial.print(" bytes free)");

  Serial.println("");
}

/**
   Arduino loop
*/
void loop() {
  if (!PacketQueue.isEmpty()) {
    Packet packet = PacketQueue.pop();
    radioSendPacket(packet);
  }
}

/**
   Callback, triggered on I2C master-write operation.
*/
void onI2CReceiveData(int byteCount) {

  while (Wire.available()) {
    unsigned char c = Wire.read();

    switch (c) {
      case 0x02: {
          memset(&ReceiveBuffer, 0, sizeof(ReceiveBuffer));
          ReceiveBuffer[0] = '\0';
          ReceiveBufferIndex = 0;
        } break;

      case 0x03: {
          ReceiveBuffer[ReceiveBufferIndex++] = '\0';

          Packet p;
          strcpy(p.Text, ReceiveBuffer);
          p.Length = strlen(p.Text);          
          PacketQueue.push(p);
        } break;

      default: {
          ReceiveBuffer[ReceiveBufferIndex++] = c;
        } break;
    }
  }
}

/**
   Callback, triggered on I2C slave-write operation.
*/
void onI2CSendData() {
}


