'use strict';

const EventEmitter = require('events');

class DataEmitter extends EventEmitter {}

const myDataEmitter = new DataEmitter();

myDataEmitter.on('data', function(data) {
  console.log(data);
  console.log(this);
});

myDataEmitter.emit('data', { id: Date.now() });
