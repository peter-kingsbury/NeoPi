const Thermistor = require('ds18b20-raspi');
const util = require('util');
const readSimpleCAsync = util.promisify(Thermistor.readSimpleC);

(async () => {
  try {
    let c = await readSimpleCAsync();
    console.log(c);
  } catch (e) {
    console.trace(e);
  }
})();
