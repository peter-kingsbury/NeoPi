'use strict';

const Promise = require('bluebird');
const i2c = require('i2c-bus');

const ARDUINO_I2C_SLAVE = 0x04;
const ACCEPTABLE_TIME_DRIFT_THRESHOLD = 10000;

let count = 0;
let failures = 0;

const buffer = new Buffer(32);

let wire = i2c.openSync(1);

function i2cread() {
  buffer.fill(0);
  return new Promise((resolve, reject) => {
    wire.i2cRead(ARDUINO_I2C_SLAVE, 32, buffer, (err, bytesRead, buffer) => {
      if (err) {
        // console.log(err.message);
        return reject(err);
      }
      resolve(buffer);
    });
  });
}

function sendCommand(byte) {
  // return Promise.resolve();
  return new Promise((resolve, reject) => {
    wire.sendByte(ARDUINO_I2C_SLAVE, byte, err => {
      if (err) {
        return reject(err);
      }
      resolve();
    });
  });
}

function run() {
  let gps = {};
  let start = Date.now();
  count++;

  return Promise.resolve().then(() => {
    return sendCommand(0x01).then(() => {
      return i2cread().then(buffer => {
        let result = buffer.toString().split('|');
        let latitude = result[0];
        let longitude = result[1];
        // console.log(`Latitude: ${latitude}  Longitude: ${longitude}`);
        gps = Object.assign(gps, { latitude, longitude });
      });
    });
  }).then(() => {
    return sendCommand(0x02).then(() => {
      return i2cread().then(buffer => {
        let result = buffer.toString().split('|');
        let speed = result[0];
        // console.log(`Speed: ${speed} m/s`);
        gps = Object.assign(gps, { speed });
      });
    });
  }).then(() => {
    return sendCommand(0x04).then(() => {
      return i2cread().then(buffer => {
        let result = buffer.toString().split('|');
        let altitude = result[0];
        // console.log(`Altitude: ${altitude} m`);
        gps = Object.assign(gps, { altitude });
      });
    });
  }).then(() => {
    return sendCommand(0x08).then(() => {
      return i2cread().then(buffer => {
        let result = buffer.toString().split('|');
        let angle = result[0];
        // console.log(`Angle: ${angle}°`);
        // console.log('');
        gps = Object.assign(gps, { angle });
      });
    }).then(() => {
      gps = Object.assign(gps, { ms: Date.now() - start });
      console.log(gps);
    });
  }).catch(err => {
    console.log('I/O error');
    // console.trace(err);
    // throw err;
    failures++;
  });
}

console.log(`Starting up...`);
setInterval(() => {
  run();
  let percent = Math.round((failures / count) * 100);
  console.log(`${percent}% failure rate (${failures} / ${count})`);
}, 100);
