const Bluebird = require('bluebird');
const i2c = Bluebird.promisifyAll(require('i2c-bus'));

const ID = 0xD0;
const RESET = 0xE0;

let bus = 1;
let address = 0x77;

let wire = i2c.openSync(bus);

// Get the chip ID
let id = wire.readByteSync()

wire.closeSync(bus);
