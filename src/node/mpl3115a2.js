'use strict';

function i2cTest() {
  const Bluebird = require('bluebird');
  const i2c = require('i2c');

  const address = 0x60;
  const cmdControlRegister1 = 0x26;
  const cmdWhoAmI = 0x0c;

  const regPressure = 0x01;
  const regTemperature = 0x04;

  let wire = Bluebird.promisifyAll(new i2c(address, {
    device: `/dev/i2c-1`
  }));

  /*
   wire.scan((err, data) => {
   if (err) {
   console.log(`err: ${err}`);
   } else {
   console.log(`data (scan): ${data}`);
   }
   });
   */

  /*who_am_i = bus.read_byte_data(ADDR, 0x0C)
   print hex(who_am_i)
   if who_am_i != 0xc4:
   print "Device not active."
   exit(1)*/

//wire.readBytes(cmdWhoAmI, 0, (err, data) => {
//  if (err) {
//    console.log(`err: ${err}`);
//  } else {
//    console.log(`data (who am i): ${data}`);
//  }
//});

  /*
   wire.on('data', data => {
   console.log(`data (evt): ${data}`);
   });

   wire.stream(regPressure, 3, 250);
   */

  wire.scanAsync().then(result => {
    console.log(`scan: ${result}`);
  }).then(() => {
    return wire.readBytesAsync(0x0C, 2).then(result => {
      console.log(result);
    });
  }).catch(err => {
    console.log(`${err.message}`);
  });
}

function i2cBusTest() {
  return new Promise((resolve, reject) => {

  });
}

i2cBusTest().then(result => {
  console.log('Done');
});
