#!/usr/bin/python

import sys
import time
import difflib
import pigpio

RX=5
TX=6

print "Welcome to PiGPIO GPS test!"

try:
  pi = pigpio.pi()
  pi.set_mode(RX, pigpio.INPUT)
  pi.bb_serial_read_open(RX, 9600, 8)
  while 1:
    (count, data) = pi.bb_serial_read(RX)
    if count:
      print data
    time.sleep(0.1)
except:
  print "Critical Error: ", sys.exc_info()[0]
  pi.bb_serial_read_close(RX)
  pi.stop()

