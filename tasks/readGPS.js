'use strict';
const { api, Task } = require('actionhero');
const Util = require('../lib/util/Util');
const ACCEPTABLE_DISTANCE_THRESHOLD = 1000;
const Home = {
  latitude: 43.383244,
  longitude: -80.310722
};
let lastGps = Home;
module.exports = class ReadGpsTask extends Task {
  constructor() {
    super();
    this.name = 'readGPS';
    this.description = 'an actionhero task';
    this.frequency = process.env.MODE === 'PAYLOAD' ? 2500 : 0;

    this.frequency = 0;

    this.queue = 'default';
    this.middleware = [];
  }

  async run(data) {
    try {
      // let start = Date.now();
      let gps = await api.sensors.GPS.read();

      await api.sensors.GPS.store(gps);
      api.readings = Object.assign(api.readings, gps);
      // api.log(`Read GPS (${Date.now() - start} ms)`);
      // api.log(`Latitude: ${api.readings.latitude} Longitude: ${api.readings.longitude}`);

      if (lastGps === null) {
        lastGps = gps;
      } else {
        let distanceFromHome = Util.geoDistance(Home.latitude, Home.longitude, gps.latitude, gps.longitude);
        distanceFromHome = Math.round(distanceFromHome * 100) / 100;
        if (distanceFromHome > ACCEPTABLE_DISTANCE_THRESHOLD) {
          // api.log(`Distance from home: ${distanceFromHome} km -  WE'RE NOT IN KANSAS ANYMORE!`, 'error');
          return;
        } else {
//          api.log(`Distance from home: ${distanceFromHome} km`, 'notice');
          let distanceDelta = Util.geoDistance(lastGps.latitude, lastGps.longitude, gps.latitude, gps.longitude);
          distanceDelta = Math.round(distanceDelta * 100) / 100;
//          api.log(`Distance since last reading: ${distanceDelta} km`, 'notice');

          // api.log(`Distance: ${distanceDelta} km (from home) ${distanceFromHome} km (delta)`);

          lastGps = gps;
        }
      }
    } catch (e) {
      api.log(this.name, 'error', e);
    }
  }
};
