'use strict';
const { api, Task } = require('actionhero');
const OS = require('os');
const Bluebird = require('bluebird');
const diskspace = Bluebird.promisifyAll(require('diskspace'));

let oldCPUTime = 0;
let oldCPUIdle = 0;

function getLoad() {
  let cpus = OS.cpus();
  let totalTime = -oldCPUTime;
  let totalIdle = -oldCPUIdle;
  for (let i = 0; i < cpus.length; i++) {
    let cpu = cpus[i];
    for (const type of Object.keys(cpu.times)) {
      totalTime += cpu.times[type];
      if (type === 'idle') {
        totalIdle += cpu.times[type];
      }
    }
  }

  oldCPUTime = totalTime;
  oldCPUIdle = totalIdle;

  return {
    CPU: 1 - (totalIdle / totalTime),
    mem: 1 - (OS.freemem() / OS.totalmem())
  };
}

async function readSystemInfo() {
  let disk = await diskspace.checkAsync('/')
  let load = getLoad();
  return {
    packetId: 0,
    name:      'System',
    sensorId:  0,
    timestamp: Date.now(),
    data: {
      cpu:    load.CPU,
      memory: load.mem,
      disk:   disk.free / disk.total
    }
  };
}

module.exports = class ReadSystemInfoTask extends Task {
  constructor() {
    super();
    this.name = 'readSystemInfo';
    this.description = 'an actionhero task';
    this.frequency = process.env.MODE === 'PAYLOAD' ? 10000 : 0;
    this.queue = 'default';
    this.middleware = [];
  }

  async run(data) {
    try {
      let start = Date.now();
      let reading = await readSystemInfo();
      // api.log(`Read System Info (${Date.now() - start} ms)`);
      await api.tasks.enqueue('transmitRadioData', reading, 'default');
    } catch (e) {
      api.log(this.name, 'error', e);
    }
  }
};
