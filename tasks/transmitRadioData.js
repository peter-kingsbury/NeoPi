'use strict';
const { api, Task } = require('actionhero');
const FrameBuilder = require('../lib/xbee/frame/FrameBuilder').FrameBuilder;
const { Packet, PACKET_ID } = require('../lib/packet/Packet');

module.exports = class TransmitRadioDataTask extends Task {
  constructor() {
    super();
    this.name = 'transmitRadioData';
    this.description = 'an actionhero task';
    this.frequency = 0;
    this.queue = 'default';
    this.middleware = [];
  }

  async run(data) {
    try {
      let fb = new FrameBuilder();

      let packet = new Packet();

      // console.log(JSON.stringify(data, null, 2));

      let buf = packet.serialize(data.data, data.packetId);

     /* console.log(`-----------------------------------`);
      console.log(data);
      console.log(buf);
      console.log(`buf.length=${buf.length} === bufstr.length=${bufstr.length} (${buf.length === bufstr.length})`);
      console.log(`buffer=${buf.toString('ascii')} length=${buf.length}`);
      console.log(`-----------------------------------`);*/

     // console.log(bufstr);

     /* let buffer = fb.build(0x10, {
        destination64: api.config.xbee.destination,
        destination16: 'fffe',
        data: JSON.stringify(Object.assign({}, data, {
          name: data.name,
          time: data.timestamp
        }))
      });*/

      let buffer = fb.build(0x10, {
        destination64: api.config.xbee.destination,
        destination16: 'fffe',
        data: buf
      });

      await api.xbee.sendBuffer(buffer);
    } catch (e) {
      api.log(`${this.name} data=${JSON.stringify(data)}`, 'error', e);
    }
  }
};
