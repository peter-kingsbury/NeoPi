'use strict';
const { api, Task } = require('actionhero');

module.exports = class ReadCompassTask extends Task {
  constructor() {
    super();
    this.name = 'readCompass';
    this.description = 'an actionhero task';
    this.frequency = process.env.MODE === 'PAYLOAD' ? 2500 : 0;

    this.frequency = 0;

    this.queue = 'default';
    this.middleware = [];
  }

  async run(data) {
    try {
      // let start = Date.now();

      let heading = await api.sensors.Compass.readHeading();
      let raw = await api.sensors.Compass.readRawValues();

      await api.sensors.Compass.store({
        heading,
        x: raw.x,
        y: raw.y,
        z: raw.z
      });

      Object.assign(api.readings, {
        heading,
        compass_x: raw.x,
        compass_y: raw.y,
        compass_z: raw.z
      });

      // api.log(`Read Compass (${Date.now() - start} ms)`);
    } catch (e) {
      api.log(this.name, 'error', e);
    }
  }
};
