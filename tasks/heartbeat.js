'use strict';
const { api, Task } = require('actionhero');
const { PACKET_ID } = require('../lib/packet/Packet');

module.exports = class MyTask extends Task {
  constructor() {
    super();
    this.name = 'heartbeat';
    this.description = 'an actionhero task';
    this.frequency = process.env.MODE === 'PAYLOAD' ? 500 : 0;

    this.frequency = 0;

    this.queue = 'default';
    this.middleware = [];
  }

  async run(data) {
    try {
      let heartbeat = {
        name:      'heartbeat',
        timestamp: Date.now(),
        data:      {}
      };

      // api.tasks.enqueue
      await api.tasks.enqueue('transmitRadioData', {
        packetId: PACKET_ID.HEARTBEAT,
        data: heartbeat
      }, 'default');
    } catch (e) {
      api.log(this.error, 'error', e);
    }
  }
};
