# N.E.O. Pi

The **N**ear - **E**arth **O**bject Raspberry **Pi** Project

### Tweet Summary ###

In 140 characters or less:

*Send a raspi into space with a helium balloon to take teddy bear selfies and measure alt/temp/pressure, then recover with radio and GPS*

### Background

Late 2016, I got the crazy idea to send a Raspberry Pi up into space as a fun project with my daughter. It would take time and lots of research to get right, and I needed to collect hardware and write software to achieve all the project's goals.

### Specifications

#### Hardware

##### The Balloon

* Raspberry Pi Zero (or 3B, if I can make its battery last long enough)
  * 8 GB MicroSD
  * 6xAA alkaline battery power
* Pi Camera V2
* BME280 (Temperature, Pressure, Altitude, Humidity) - internal
* KY-013 (Temperature) - external
* LIS3DH (Accelerometer x,y,z)
* TSL2591 (Luminosity)
* HMC5883L (x,y,z in uT, heading in degrees)
* SI1145 (Visibility, IR, Proximity, UV Level)
* DS3231SN Real-Time Clock
* Ultimate GPS V3
* XBee-PRO 900HP radio transmitter
* Arduino Pro Mini used as a controller for the radio
* Our mascot, Captain Polaris, a [Space Teddy Bear](http://ep.yimg.com/ay/nasa/nasa-mission-crew-stuffed-bear-orange-12.png)
* An incredibly light frame that can also handle impact on landing
* A helium balloon
* Helium (for the balloon)

##### Mission Control

* MacBook Pro
* XBee-PRO 900HP radio transmitter
* OpenStreetMaps

#### Software

* Run a process which can invoke various functions available on the Pi.
* For any software written, publish on the web for the benefit of others.
* Read sensors on a predetermined interval:
    * Barometric pressure
    * Altitude
    * Temperature
    * GPS coordinates
* Capture images at either (1) a set interval (e.g. every 30 seconds), or (2) given a certain altitude (e.g. every 100 metres).
* Monitor battery usage, and ease off on the data collection interval if we drop below a certain threshold (say, 50%).
* Broadcast as much as we can via radio.
* Record everything in a locally-stored database.
* Be prepared to send Node.js code into space!

##### Example

With a 600g balloon, a 500g payload and 500g lift, we would need ~57.5 ft3 of helium, expect the balloon to burst at ~30190 metres, ascend at a rate of ~4.4 m/s, and for the entire ascent to take ~115 minutes.

#### Questions

##### Are you affiliated with NeoPi over on GitHub?

Nope. I'm not sure why they're using "Neo", but I use NeoPi independent of the concept of malware detection software (their bag). I prefer its use for the acronym, **Near Earth Object**.

##### Isn't it a little presumptuous to send a teddy bear into space?

Totally. My daughter is a big fan of teddy bears, and since this is a quality-time educational project with her, it's only fitting that she have the opportunity to add a bear that's been *into space* to her collection.

### Reference Documents

[MPL3115A2](http://cdn.sparkfun.com/datasheets/Sensors/Pressure/MPL3115A2.pdf)

[MPL3115A2 in Python](http://ciaduck.blogspot.ca/2014/12/mpl3115a2-sensor-with-raspberry-pi.html)

[i2c-bus](https://github.com/fivdi/i2c-bus)

[i2c-bus (npmjs.org)](https://www.npmjs.com/package/i2c-bus)

[XBee Radios Point2Point](https://learn.adafruit.com/xbee-radios/point2point)

[XBee for Arduino and Raspberry Pi](https://www.cooking-hacks.com/documentation/tutorials/xbee-arduino-raspberry-pi-tutorial/)

### References

[LIS3DH](https://learn.adafruit.com/adafruit-lis3dh-triple-axis-accelerometer-breakout?view=all)

[Ultimate GPS](https://learn.adafruit.com/adafruit-ultimate-gps/downloads?view=all)

[Ultimate GPS on the Raspberry Pi](https://learn.adafruit.com/adafruit-ultimate-gps-on-the-raspberry-pi?view=all)

[node-gpsd](https://github.com/eelcocramer/node-gpsd)
[BMP280](https://learn.adafruit.com/adafruit-bmp280-barometric-pressure-plus-temperature-sensor-breakout?view=all)

[BMP280 in Python](https://github.com/ControlEverythingCommunity/BMP280/blob/master/Python/BMP280.py)

[DS3231](https://learn.adafruit.com/adafruit-ds3231-precision-rtc-breakout/overview)

[DS3231 Technical Manual](https://web.wpi.edu/Pubs/E-project/Available/E-project-010908-124414/unrestricted/DS3231-DS3231S.pdf)

[RFM9x LoRa Transceiver](https://learn.adafruit.com/adafruit-rfm69hcw-and-rfm96-rfm95-rfm98-lora-packet-padio-breakouts?view=all)

### Links

[Balloon Performance Calculator](http://tools.highaltitudescience.com/)

[Raspberry Pi Specifications](https://en.wikipedia.org/wiki/Raspberry_Pi#Specifications)

[Pi in the Sky](https://www.raspberrypi.org/blog/pi-in-the-sky-2/)

[Raspberry Pi Camera v2 Product Specifications](http://www.avid-tech.com/downloads/Product%20Spec%20Sheet/Pi-camera-v2.pdf)

[Radiometrix NT2 model transmitter](http://www.radiometrix.com/content/ntx2)

[Raspberry Pi Power Switch](http://www.raspberry-pi-geek.com/Archive/2013/01/Adding-an-On-Off-switch-to-your-Raspberry-Pi)

[Raspberry Pi Camera Multiplexer](http://www.ivmech.com/magaza/en/development-modules-c-4/ivport-v2-raspberry-pi-camera-module-v2-multiplexer-p-107)