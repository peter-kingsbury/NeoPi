'use strict';

process.env.NODE_ENV = 'test';

const should = require('should'); // eslint-disable-line
const XBee = require('../lib/xbee/XBee').XBee;
const FrameBuilder = require('../lib/xbee/frame/FrameBuilder').FrameBuilder;
let dev = `/dev/cu.usbserial-DN02N149`;

describe('FrameBuilder tests', function() {
  it('should be able to create a frame', () => {
    let fb = new FrameBuilder();
    let buffer = fb.build(0x10, {
      destination64: '0013A200415B3CA7',
      destination16: 'fffe',
      data: 'Hello, World!'
    });

    buffer.length.should.equal(31);
  });

  it('should be able to reject unsupported frame types', () => {
    try {
      let fb = new FrameBuilder();
      fb.build(0xff, {
        destination64: '0013A200415B3CA7',
        destination16: 'fffe',
        data: 'this does not work'
      });
    } catch (e) {
      e.message.should.equal(`Unsupported frame type 255`);
      return;
    }

    throw new Error(`failed to reject unsupported frame types`);
  });

  it('should be able to reject a frame whose payload exceeds 256 bytes', () => {
    try {
      let longText = `01234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456`;
      let fb = new FrameBuilder();
      let buffer = fb.build(0x10, {
        destination64: '0013A200415B3CA7',
        destination16: 'fffe',
        data: longText
      });
      buffer.length.should.equal(31);
    } catch (e) {
      e.message.should.equal(`data.data (length 257) exceeded maximum length of 256`);
      return;
    }

    throw new Error(`failed to reject a frame whose payload exceeds 256 bytes`);
  });
});

describe('XBee tests', function() {
  it('should be able to send a frame', done => {
    let fb = new FrameBuilder();
    let buffer = fb.build(0x10, {
      destination64: '0013A200415B3CA7',
      destination16: 'fffe',
      data: 'Hello, World!'
    });

    let xbee = new XBee(dev, 115200);
    xbee.initialize().then(result => {
      return xbee.delay(100).then(() => {
        return xbee.writeRaw(buffer.toString('hex')).then(() => {
          return xbee.close();
        });
      });
    }).then(done).catch(done);
  }).timeout(5000);

  it('should be able to enter command mode', done => {
    let xbee = new XBee(dev, 115200);
    xbee.initialize().then(result => {
      return xbee.enterCommandMode();
    }).then(done).catch(done);
  }).timeout(60000);
});
