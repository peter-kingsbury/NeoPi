'use strict';

const mocha = require('mocha');
const should = require('should');

const { Packet, PACKET_ID } = require('../lib/packet/Packet');

describe(`Packet Unit Tests`, () => {
  before(() => {});

  after(() => {});

  it(`Accelerometer`, () => {
    let p = new Packet();
    let buf = p.serialize({
      accelX: 1.2345, accelY: 2.3456, accelZ: 3.4567
    }, PACKET_ID.ACCELEROMETER);

    buf.length.should.equal(13);

    let obj = p.deserialize(buf);
    obj.accelX.should.be.approximately(1.2345, 0.001);
    obj.accelY.should.be.approximately(2.3456, 0.001);
    obj.accelZ.should.be.approximately(3.4567, 0.001);
  });

  it(`Environment`, () => {
    let p = new Packet();
    let buf = p.serialize({
      temperature: 1.2345, humidity: 2.3456, pressure: 3.4567, altitude: 4.5678
    }, PACKET_ID.ENVIRONMENT);

    buf.length.should.equal(17);

    let obj = p.deserialize(buf);
    obj.temperature.should.be.approximately(1.2345, 0.001);
    obj.humidity.should.be.approximately(2.3456, 0.001);
    obj.pressure.should.be.approximately(3.4567, 0.001);
    obj.altitude.should.be.approximately(4.5678, 0.001);
  });

  it(`Temperature`, () => {
    let p = new Packet();
    let buf = p.serialize({
      temperature: 1.2345
    }, PACKET_ID.TEMPERATURE);

    buf.length.should.equal(5);

    let obj = p.deserialize(buf);
    obj.temperature.should.be.approximately(1.2345, 0.001);
  });

  it(`GPS`, () => {
    let p = new Packet();
    let buf = p.serialize({
      latitude: 1.2345,
      longitude: 2.3456,
      speed: 3.4567,
      altitude: 4.5678,
      angle: 5.6789
    }, PACKET_ID.GPS);

    buf.length.should.equal(21);

    let obj = p.deserialize(buf);
    obj.latitude.should.be.approximately(1.2345, 0.001);
    obj.longitude.should.be.approximately(2.3456, 0.001);
    obj.speed.should.be.approximately(3.4567, 0.001);
    obj.altitude.should.be.approximately(4.5678, 0.001);
    obj.angle.should.be.approximately(5.6789, 0.001);
  });

  it(`Compass`, () => {
    let p = new Packet();
    let buf = p.serialize({
      heading: 1.2345,
      x: 2.3456,
      y: 3.4567,
      z: 4.5678
    }, PACKET_ID.COMPASS);

    buf.length.should.equal(17);

    let obj = p.deserialize(buf);
    obj.heading.should.be.approximately(1.2345, 0.001);
    obj.x.should.be.approximately(2.3456, 0.001);
    obj.y.should.be.approximately(3.4567, 0.001);
    obj.z.should.be.approximately(4.5678, 0.001);
  });

  it(`Ultraviolet`, () => {
    let p = new Packet();
    let buf = p.serialize({
      uv: 1.2345
    }, PACKET_ID.ULTRAVIOLET);

    buf.length.should.equal(5);

    let obj = p.deserialize(buf);
    obj.uv.should.be.approximately(1.2345, 0.001);
  });

  it(`Luminosity`, () => {
    let p = new Packet();
    let buf = p.serialize({
      visibility: 1.2345,
      ir: 2.3456
    }, PACKET_ID.LUMINOSITY);

    buf.length.should.equal(9);

    let obj = p.deserialize(buf);
    obj.visibility.should.be.approximately(1.2345, 0.001);
    obj.ir.should.be.approximately(2.3456, 0.001);
  });
});
